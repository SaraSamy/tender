package com.tender.model;

import java.io.Serializable;

public class Tender implements Serializable {
    String id;
    String title;
    String disc;
    int totalPoints;
    String img;
    String type;
    String closedFor;
    String closedForName;

    public String getClosedForName() {
        return closedForName;
    }

    public void setClosedForName(String closedForName) {
        this.closedForName = closedForName;
    }

    public String getClosedFor() {
        return closedFor;
    }

    public void setClosedFor(String closedFor) {
        this.closedFor = closedFor;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Tender() {
    }

    @Override
    public String toString() {
        return "Tender{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", disc='" + disc + '\'' +
                ", totalPoints=" + totalPoints +
                ", img='" + img + '\'' +
                ", type='" + type + '\'' +
                ", closedFor='" + closedFor + '\'' +
                '}';
    }

    public Tender(String id, String title, String disc, int totalPoints, String img, String type, String closedFor) {
        this.id = id;
        this.title = title;
        this.disc = disc;
        this.totalPoints = totalPoints;
        this.img = img;
        this.type = type;
        this.closedFor = closedFor;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDisc() {
        return disc;
    }

    public void setDisc(String disc) {
        this.disc = disc;
    }

    public int getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(int totalPoints) {
        this.totalPoints = totalPoints;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
