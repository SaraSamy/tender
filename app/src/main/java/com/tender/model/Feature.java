package com.tender.model;

import java.io.Serializable;

public class Feature implements Serializable {
    String title;
    String feature;
    int points;
    String tenderId;
    String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Feature{" +
                "title='" + title + '\'' +
                ", feature='" + feature + '\'' +
                ", points=" + points +
                ", tenderId='" + tenderId + '\'' +
                '}';
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    public String getFeature() {
        return feature;
    }

    public String getTitle() {
        return title;
    }

    public Feature() {
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPoints() {
        return points;
    }

    public String getTenderId() {
        return tenderId;
    }

    public void setTenderId(String tenderId) {
        this.tenderId = tenderId;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public Feature(String title, String feature, int points, String tenderId,String fId) {
        this.title = title;
        this.feature = feature;
        this.points = points;
        this.tenderId = tenderId;
        this.id=fId;
    }
}
