package com.tender.model;

import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class Offer implements Comparable {
    String offerId;
    int points;
    String tId;
    String addedId;
    String[] featureArray;
    Map<String, Integer> featuresPoints = new HashMap();
    private ArrayList<Feature> feature =new ArrayList<>();
    Double price;
    String addedImg;
    String addedName;
    String file;
    int offerFP;

    public int getOfferFP() {
        return offerFP;
    }

    public void setOfferFP(int offerFP) {
        this.offerFP = offerFP;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }


    public void setFeaturesPoints(Map<String, Integer> featuresPoints) {
        this.featuresPoints = featuresPoints;
    }

    boolean hasFeature(String feature) {
        return getFeaturesPoints().containsKey(feature);
    }

    private Map<String, Integer> getFeaturesPoints() {
        if(feature == null || feature.isEmpty()) return new HashMap();
        Log.e("wwww",String.valueOf(feature.size()));
        for (Feature featureEnty:feature) {
            if(!featuresPoints.containsKey(featureEnty.feature))
                featuresPoints.put(featureEnty.feature, 0);

            featuresPoints.put(featureEnty.feature, featureEnty.points + featuresPoints.get(featureEnty.feature));
        }
        Log.e("wwww",feature.toString());
        return featuresPoints;
    }

    public int compareTo(Object offer, String feature) {
        int comparePoints = 0;
        int myPoints = 0;
        if(((Offer)offer).getFeaturesPoints().containsKey(feature))
            comparePoints = ((Offer)offer).getFeaturesPoints().get(feature);

        if(getFeaturesPoints().containsKey(feature))
            myPoints = getFeaturesPoints().get(feature);
        ((Offer)offer).setOfferFP(myPoints);
        Log.e("wwww",String.valueOf( getFeaturesPoints()));


        return comparePoints- myPoints;
    }

    public ArrayList<Feature> getFeature() {
        return feature;
    }

    public void setFeature(ArrayList<Feature> feature) {
        this.feature = feature;
    }

    public String getAddedImg() {
        return addedImg;
    }

    public void setAddedImg(String addedImg) {
        this.addedImg = addedImg;
    }

    public String getAddedName() {
        return addedName;
    }

    public void setAddedName(String addedName) {
        this.addedName = addedName;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Offer() {
    }

    public Offer(String offerId, int points, String tId, String addedId, String[] featureArray,
                 ArrayList<Feature> feature, Double price, String addedImg, String addedName,String file) {
        this.offerId = offerId;
        this.points = points;
        this.tId = tId;
        this.addedId = addedId;
        this.featureArray = featureArray;
        this.feature = feature;
        this.price = price;
        this.addedImg = addedImg;
        this.addedName = addedName;
        this.file=file;
    }

    @Override
    public String toString() {
        return "Offer{" +
                "offerId='" + offerId + '\'' +
                ", points=" + points +
                ", tId='" + tId + '\'' +
                ", addedId='" + addedId + '\'' +
                ", featureArray=" + Arrays.toString(featureArray) +
                ", price=" + price +
                ", addedImg='" + addedImg + '\'' +
                ", addedName='" + addedName + '\'' +
                '}';
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public String gettId() {
        return tId;
    }

    public void settId(String tId) {
        this.tId = tId;
    }

    public String getAddedId() {
        return addedId;
    }

    public void setAddedId(String addedId) {
        this.addedId = addedId;
    }

    public String[] getFeatureArray() {
        return featureArray;
    }

    public void setFeatureArray(String[] featureArray) {
        this.featureArray = featureArray;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    @Override
    public int compareTo(Object offer) {
//        this.offerFP=this.points;
        int comparePoints=((Offer)offer).getPoints();
        return comparePoints-this.points;
    }
}
