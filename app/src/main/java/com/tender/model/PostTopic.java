package com.tender.model;

public class PostTopic<T> {
    String to;
    NotificationModel notification;
    T data;

    public PostTopic(String to, NotificationModel notification, T data) {
        this.to = to;
        this.notification = notification;
        this.data = data;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String topic) {
        this.to = topic;
    }

    public NotificationModel getNotification() {
        return notification;
    }

    public void setNotification(NotificationModel notification) {
        this.notification = notification;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "PostTopic{" +
                "to='" + to + '\'' +
                ", notification=" + notification +
                ", data=" + data +
                '}';
    }

    public static class NotificationModel {
        String title;
        String text;

        public NotificationModel(String title, String text) {
            this.title = title;
            this.text = text;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return "NotificationModel{" +
                    "title='" + title + '\'' +
                    ", text='" + text + '\'' +
                    '}';
        }
    }
}
