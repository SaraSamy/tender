package com.tender.model;

public class PriceFeature {
    int points;
    int from;
    int to;
    String tenderId;
    String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "PriceFeature{" +
                "points=" + points +
                ", from=" + from +
                ", to=" + to +
                ", tenderId='" + tenderId + '\'' +
                ", id='" + id + '\'' +
                '}';
    }

    public PriceFeature(){}

    public String getTenderId() {
        return tenderId;
    }

    public void setTenderId(String tenderId) {
        this.tenderId = tenderId;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public PriceFeature(int points, int from, int to, String tenderId, String id) {
        this.points = points;
        this.from = from;
        this.to = to;
        this.tenderId = tenderId;
        this.id = id;
    }
}
