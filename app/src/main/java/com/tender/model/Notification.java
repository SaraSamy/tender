package com.tender.model;

public class Notification {
    String title;

    public Notification(String title, String body, String type, String tender) {
        this.title = title;
        this.body = body;
        this.type = type;
        this.tender = tender;
    }

    String body;
    String type;

    public Notification() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTender() {
        return tender;
    }

    public void setTender(String tender) {
        this.tender = tender;
    }

    @Override
    public String toString() {
        return "Notification{" +
                "title='" + title + '\'' +
                ", body='" + body + '\'' +
                ", type='" + type + '\'' +
                ", tender='" + tender + '\'' +
                '}';
    }

    String tender;
}
