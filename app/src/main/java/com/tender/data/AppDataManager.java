package com.tender.data;

import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.storage.UploadTask;
import com.tender.data.firebase.AppFirebaseHelper;
import com.tender.data.firebase.FirebaseHelper;
import com.tender.model.Feature;
import com.tender.model.Offer;
import com.tender.model.PriceFeature;
import com.tender.model.User;
import com.tender.ui.base.BaseLisener;

import java.util.ArrayList;
import java.util.Objects;

public class AppDataManager implements DataManager {

    private FirebaseHelper firebaseAppHelper;

    public AppDataManager() {
        firebaseAppHelper = new AppFirebaseHelper();
    }

    @Override
    public void signUp(String email, String password, final BaseLisener<String, String, String> lisener, User user) {
        firebaseAppHelper.signUp(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> taskAuth) {
                if (taskAuth.isSuccessful()) {
                    firebaseAppHelper.postUser(taskAuth.getResult().getUser().getUid(), user).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                FirebaseMessaging.getInstance().subscribeToTopic("/topics/tenders")
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
//                                                if (!task.isSuccessful()) {
//
//                                                }
                                                lisener.onSuccess(taskAuth.getResult().getUser().getUid());
                                            }
                                        });
                            } else {
                                lisener.onFail(task.getException().getMessage());
                            }
                        }
                    });
                } else
                    lisener.onFail(taskAuth.getException().getMessage());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                lisener.onFail(e.getMessage());
            }
        });
    }

    @Override
    public void login(String email, String password, final BaseLisener<User, String, String> lisener) {
        firebaseAppHelper.login(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> taskAuth) {
                if (taskAuth.isSuccessful()) {
                    FirebaseMessaging.getInstance().subscribeToTopic("/topics/tenders")
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                }
                            });
                    firebaseAppHelper.getUser(taskAuth.getResult().getUser().getUid(), lisener);
                } else
                    lisener.onFail(taskAuth.getException().getMessage());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                lisener.onFail(e.getMessage());
            }
        });
    }

    @Override
    public void getUser(String uId, BaseLisener<User, String, String> lisener) {
        firebaseAppHelper.getUser(uId, lisener);
    }

    @Override
    public Task<Void> updateUser(User user, BaseLisener<User, String, String> lisener) {
        return firebaseAppHelper.updateUser(user, lisener).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    lisener.onSuccess(user);
                } else
                    lisener.onFail(task.getException().getMessage());
            }
        });
    }

    @Override
    public void getData(String endPoint, String query, String equalTo, BaseLisener lisener, Class classType) {
        firebaseAppHelper.getData(endPoint, query, equalTo, lisener, classType);
    }

    @Override
    public void search(String endPoint, String query, String equalTo, BaseLisener lisener, Class classType) {
        firebaseAppHelper.search(endPoint, query, equalTo, lisener, classType);
    }

    @Override
    public void postData(String endPoint, String key, Object data, BaseLisener lisener, ArrayList<PriceFeature> priceFeatures, ArrayList<Feature> features) {
        firebaseAppHelper.postData(endPoint, key, data).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    if (priceFeatures == null && features == null)
                        lisener.onSuccessData(data, key);
                    else if (priceFeatures != null && features != null) {
                        Feature feature;
                        for (int x = 0; x < features.size(); x++) {
                            feature = features.get(x);
                            feature.setTenderId(key);
                            firebaseAppHelper.postData(
                                    "features",
                                    FirebaseDatabase.getInstance().getReference().push().getKey(),
                                    feature).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                }
                            });
                        }
                        PriceFeature priceFeature;
                        for (int x = 0; x < priceFeatures.size(); x++) {
                            priceFeature = priceFeatures.get(x);
                            priceFeature.setTenderId(key);
                            firebaseAppHelper.postData(
                                    "priceFeatures",
                                    FirebaseDatabase.getInstance().getReference().push().getKey(),
                                    priceFeature).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                }
                            });
                        }
                        lisener.onSuccessData(data, key);
                    } else {
                        if (features != null) {
                            Feature feature;
                            for (int x = 0; x < features.size(); x++) {
                                feature = features.get(x);
                                feature.setTenderId(key);
                                firebaseAppHelper.postData(
                                        "features",
                                        FirebaseDatabase.getInstance().getReference().push().getKey(),
                                        feature).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {

                                    }
                                });
                            }
                            lisener.onSuccessData(data, key);
                        }
                        if (priceFeatures != null) {
                            PriceFeature priceFeature;
                            for (int x = 0; x < priceFeatures.size(); x++) {
                                priceFeature = priceFeatures.get(x);
                                priceFeature.setTenderId(key);
                                firebaseAppHelper.postData(
                                        "priceFeatures",
                                        FirebaseDatabase.getInstance().getReference().push().getKey(),
                                        priceFeature).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                    }
                                });
                            }
                            lisener.onSuccessData(data, key);
                        }
                    }
                } else {
                    lisener.onFail(Objects.requireNonNull(task.getException()).getMessage());
                }
            }
        });
    }

    @Override
    public void updateData(String endPoint, String key, Object data, BaseLisener lisener) {
        firebaseAppHelper.postData(endPoint, key, data).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    lisener.onSuccessData(data, key);
                } else {
                    lisener.onFail(Objects.requireNonNull(task.getException()).getMessage());
                }
            }
        });
    }

    @Override
    public UploadTask uploadImage(Uri img) {
        return firebaseAppHelper.uploadImage(img);
    }

    @Override
    public void postNotification(Object data) {
        firebaseAppHelper.postNotification(data).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Log.e("postNotification", data.toString());
                } else {
                    Log.e("postNotification", Objects.requireNonNull(task.getException()).getMessage());
                }
            }
        });
    }

    @Override
    public void postOffer(Offer data, ArrayList<Feature> featureList, BaseLisener lisener) {
        FirebaseDatabase.getInstance().getReference()
                .child("priceFeatures").orderByChild("tenderId").equalTo(data.gettId())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() != null) {
                            for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                                Log.e("price", childDataSnapshot.child("from").getValue().toString());
                                if (data.getPrice() >= Integer.parseInt(childDataSnapshot.child("from").getValue().toString())
                                        && data.getPrice() < Integer.parseInt(childDataSnapshot.child("to").getValue().toString())) {
                                    data.setPoints(data.getPoints() + Integer.parseInt(childDataSnapshot.child("points").getValue().toString()));
                                }
                            }
                            String key = FirebaseDatabase.getInstance().getReference().push().getKey();
                            firebaseAppHelper.postData("offers", key, data).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        lisener.onSuccess(data);
                                    } else {
                                        lisener.onFail(Objects.requireNonNull(task.getException()).getMessage());
                                    }
                                }
                            });
                        } else {

                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    @Override
    public void getOffer(String endPoint, String query, String equalTo, BaseLisener lisener, Class classType) {
        FirebaseDatabase.getInstance().getReference().child(endPoint).orderByChild(query).equalTo(equalTo)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() != null) {
                            for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                                Log.e("aaaaaa", childDataSnapshot.child("addedId").getValue().toString());
                                FirebaseDatabase.getInstance().getReference()
                                        .child("users").child(childDataSnapshot.child("addedId").getValue().toString())
                                        .addValueEventListener(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                if (dataSnapshot.getValue() != null) {
                                                    Offer offer = childDataSnapshot.getValue(Offer.class);
                                                    offer.setAddedImg(dataSnapshot.child("img").getValue().toString());
                                                    offer.setAddedName(dataSnapshot.child("name").getValue().toString());
                                                    lisener.onSuccessData(offer, childDataSnapshot.getKey());
                                                } else
                                                    lisener.onFail("no data");
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                                lisener.onFail(databaseError.getMessage());
                                            }
                                        });
                            }
                        } else {
                            lisener.onFail("no data");
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }
}



