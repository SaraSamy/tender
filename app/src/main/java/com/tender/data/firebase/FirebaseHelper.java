package com.tender.data.firebase;

import android.net.Uri;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.storage.UploadTask;
import com.tender.model.User;
import com.tender.ui.base.BaseLisener;

public interface FirebaseHelper {

    Task<AuthResult> signUp(String email, String password);

    Task<AuthResult> login(String email, String password);

    Task<Void>  postUser(String uId, User user);

    void  getUser(String uId, BaseLisener<User, String, String> lisener);

    Task<Void>  updateUser(User user, BaseLisener<User, String, String> lisener);

    void getData(String endPoint, String query, String equalTo, BaseLisener lisener, Class classType);

    Task <Void> postData(String endPoint, String key, Object data);

    Task <Void> postNotification(Object data);

    UploadTask uploadImage(Uri img);

    void search(String endPoint, String query, String startAt, BaseLisener lisener, Class classType);
}
