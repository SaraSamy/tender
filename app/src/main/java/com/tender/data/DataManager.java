package com.tender.data;

import android.net.Uri;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.UploadTask;
import com.tender.model.Feature;
import com.tender.model.Offer;
import com.tender.model.PriceFeature;
import com.tender.model.User;
import com.tender.ui.base.BaseLisener;
import java.util.ArrayList;

public interface DataManager {

    void signUp(String email, String password, BaseLisener<String, String,String> lisener, User user);

    void login(String email, String password, BaseLisener<User, String,String> lisener);

    void getUser(String uId, BaseLisener<User, String,String> lisener);

    Task<Void> updateUser(User user, BaseLisener<User, String,String> lisener);

    void getData(String endPoint,String query,String equalTo,BaseLisener lisener, Class classType);

    void search(String endPoint,String query,String equalTo,BaseLisener lisener, Class classType);

    void postData (String endPoint, String key, Object data, BaseLisener lisener, ArrayList<PriceFeature> PriceFeatures, ArrayList<Feature> features);

    void updateData (String endPoint, String key, Object data, BaseLisener lisener);

    UploadTask uploadImage(Uri img);

    void postNotification(Object data);

    void postOffer(Offer data, ArrayList<Feature> featureList , BaseLisener lisener);

    void getOffer(String endPoint,String query,String equalTo,BaseLisener lisener, Class classType);
}
