package com.tender.ui.companies;

import com.tender.R;
import com.tender.data.DataManager;
import com.tender.model.Tender;
import com.tender.model.User;
import com.tender.ui.base.BaseLisener;
import com.tender.ui.base.BasePresenter;
import com.tender.ui.tenders.TenderContract;

public class AllCompaniesPresenter<V extends AllCompaniesContract.View> extends BasePresenter<V>
        implements AllCompaniesContract.Presenter<V> , BaseLisener<User, String,String> {

    public AllCompaniesPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void onSuccess(User data) {

    }

    @Override
    public void onSuccessData(User data, String key) {
        if (isViewAttached()) {
            data.setId(key);
            getMvpView().hideLoading();
            getMvpView().userCome(data);

        }
    }

    @Override
    public void onFail(String error) {
        if (isViewAttached()) {
            getMvpView().hideLoading();
            if (error.equals("no data")) {
                getMvpView().empty();
            } else
                getMvpView().showMessage(error);
        }
    }

    @Override
    public void getUser() {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            getDataManager().getData("users", "type", "company", this, User.class);
        } else {
            getMvpView().showMessage(R.string.error_no_iternet_connection);
        }
    }
}
