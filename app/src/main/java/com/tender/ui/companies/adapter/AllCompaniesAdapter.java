package com.tender.ui.companies.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.recyclerview.widget.RecyclerView;

import com.tender.R;
import com.tender.model.Tender;
import com.tender.model.User;
import java.util.ArrayList;
import java.util.List;

public class AllCompaniesAdapter extends RecyclerView.Adapter<AllCompaniesVH> implements Filterable {

    private ArrayList<User> users;
    private UserData userData;
    private List<User>filteredData = null;


    public AllCompaniesAdapter(ArrayList<User> users,  UserData userData) {
        this.users = users;
        this.filteredData=users;
        this.userData=userData;
    }

    @Override
    public AllCompaniesVH onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AllCompaniesVH(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_user, null));
    }

    @Override
    public void onBindViewHolder(AllCompaniesVH holder, int position) {
        holder.bind(users.get(position));
        holder.itemView.setOnClickListener(view -> {
            userData.userView(users.get(position));
        });
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public void addCompany(User  user) {
        if (user != null) {
            users.add(user);
            notifyDataSetChanged();
        }
    }

    public void updateItem( User  user) {
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).equals(user)) {
                users.set(i, user);
                notifyDataSetChanged();
                return;
            }
        }
    }

    public void deleteItem(Tender tender){
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).equals(tender)) {
                users.remove(i);
                notifyDataSetChanged();
                return;
            }
        }
    }

    public void addCompanies(ArrayList< User> users) {
        if (users != null) {
            this.users.addAll(users);
            notifyDataSetChanged();
        }
    }

    public ArrayList< User> getTender() {
        return users;
    }

    @Override
    public Filter getFilter() {
        return new Filter(){
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String filterString = constraint.toString().toLowerCase();

                FilterResults results = new FilterResults();

                final List<User> list = users;
                final ArrayList<User> nlist = new ArrayList<User>(list.size());

                String filterableString ;

                for (int i = 0; i < list.size(); i++) {
                    filterableString = list.get(i).getName();
                    Log.e("ffff",filterableString);
                    if (filterableString.toLowerCase().contains(filterString)) {
                        nlist.add( list.get(i));
                    }
                }

                results.values = nlist;
                results.count = nlist.size();

                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults results) {
                users = (ArrayList<User>) results.values;
                notifyDataSetChanged();
            }
        };
    }


    public interface UserData {
        void userView( User user);
    }
}
