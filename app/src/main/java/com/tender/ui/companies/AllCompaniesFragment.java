package com.tender.ui.companies;


import android.os.Bundle;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tender.R;
import com.tender.data.AppDataManager;
import com.tender.model.User;
import com.tender.ui.base.BaseFragment;
import com.tender.ui.companies.adapter.AllCompaniesAdapter;
import com.tender.ui.companies.company.CompanyFragment;
import com.tender.utils.ViewUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import gun0912.tedbottompicker.GridSpacingItemDecoration;
import kotlin.jvm.internal.Intrinsics;

public class AllCompaniesFragment extends BaseFragment implements AllCompaniesContract.View, AllCompaniesAdapter.UserData {

    @BindView(R.id.companyRV)
    RecyclerView companyRV;
    @BindView(R.id.search)
    SearchView search;
    @BindView(R.id.NoDataIV)
    ImageView NoDataIV;
    @BindView(R.id.NoDataTV)
    TextView NoDataTV;
    AllCompaniesAdapter adapter;
    AllCompaniesPresenter presenter;
    private ArrayList<User> companies = new ArrayList<>();

    public AllCompaniesFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_all_companies, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void setUp(View view) {
        companies = new ArrayList<>();
        adapter = new AllCompaniesAdapter(companies, this);
        presenter = new AllCompaniesPresenter(new AppDataManager());
        presenter.onAttach(this);
        presenter.getUser();
        setUpRecyclers(companyRV);

        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                    adapter.getFilter().filter(newText);
                return false;
            }
        });
    }

    private void setUpRecyclers(@NotNull RecyclerView recyclerView) {
        GridLayoutManager girdLayoutManager = new GridLayoutManager(this.getContext(), 1);
        recyclerView.setLayoutManager(girdLayoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration((new GridSpacingItemDecoration(1, ViewUtils.dpToPx(8.0F), true)));
    }

    @Override
    public void userCome(User user) {
        View view = getView();
        if (view != null) {
            view.findViewById(R.id.NoDataTV).setVisibility(View.GONE);
            view.findViewById(R.id.NoDataIV).setVisibility(View.GONE);
            view.findViewById(R.id.companyRV).setVisibility(View.VISIBLE);
        }
        adapter.addCompany(user);
    }

    @Override
    public void empty() {
        View view = getView();
        if (view != null) {
            view.findViewById(R.id.NoDataTV).setVisibility(View.VISIBLE);
            view.findViewById(R.id.NoDataIV).setVisibility(View.VISIBLE);
            view.findViewById(R.id.companyRV).setVisibility(View.GONE);
        }
    }

    @Override
    public void userView(User user) {
        getBaseActivity().replaceFragmentToActivity(CompanyFragment.newInstance("", user), true);
    }


    public static AllCompaniesFragment newInstance(@NotNull String title) {
        Intrinsics.checkParameterIsNotNull(title, "title");
        return new AllCompaniesFragment();
    }

}
