package com.tender.ui.companies.adapter;


import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.tender.R;
import com.tender.model.Tender;
import com.tender.model.User;

import butterknife.BindView;
import butterknife.ButterKnife;


class AllCompaniesVH extends RecyclerView.ViewHolder {

    @BindView(R.id.nameTV)
    TextView nameTV;
    @BindView(R.id.img)
    ImageView img;

    AllCompaniesVH(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind( User user) {
        nameTV.setText(user.getName());
        if(user.getImg()!=null){
            Glide.with(itemView.getContext()).load(user.getImg()).into(img);
        }
    }
}
