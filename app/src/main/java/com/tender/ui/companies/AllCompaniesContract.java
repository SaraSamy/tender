package com.tender.ui.companies;

import com.tender.model.Tender;
import com.tender.model.User;
import com.tender.ui.base.MvpPresenter;
import com.tender.ui.base.MvpView;

public interface AllCompaniesContract {
     interface View extends MvpView {
        void userCome(User user);
         void empty();
    }

    interface Presenter<V extends View> extends MvpPresenter<V> {
         void getUser();
    }
}
