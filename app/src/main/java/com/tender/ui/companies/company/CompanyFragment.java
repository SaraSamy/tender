package com.tender.ui.companies.company;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.res.FontResourcesParserCompat;

import com.bumptech.glide.Glide;
import com.tender.R;
import com.tender.model.User;
import com.tender.ui.base.BaseFragment;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;
import butterknife.ButterKnife;
import kotlin.jvm.internal.Intrinsics;

public class CompanyFragment extends BaseFragment {


    @BindView(R.id.userTIV)
    ImageView userTIV;

    public CompanyFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_company, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    public static CompanyFragment newInstance(@NotNull String title, User user) {
        Intrinsics.checkParameterIsNotNull(title, "title");
        Bundle bundle = new Bundle();
        bundle.putSerializable("user", user);
        CompanyFragment fragment = new CompanyFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected void setUp(View view) {
        Bundle bundle = getArguments();
        User user = (User) bundle.getSerializable("user");
        Glide.with(this).load(R.drawable.background).into((ImageView) view.findViewById(R.id.userTIV));
        if (user != null) {
            if (user.getImg() != null) {
                Glide.with(this).load(user.getImg()).into((ImageView) view.findViewById(R.id.userIV));
            }
            TextView name = view.findViewById(R.id.nameTV);
            name.setText(user.getName());

            if (user.getBio() != null && !(user.getBio().equals(""))) {
                TextView bio = view.findViewById(R.id.bio);
                bio.setText(user.getBio());
            }

            if (user.getEmail() != null && !(user.getEmail().equals(""))) {
                TextView email = view.findViewById(R.id.email);
                email.setText(user.getEmail());
            }
            if (user.getAddress() != null && !(user.getAddress().equals(""))) {
                TextView location = view.findViewById(R.id.location);
                location.setText(user.getAddress());
            }
        }
    }
}
