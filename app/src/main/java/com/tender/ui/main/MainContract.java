package com.tender.ui.main;

import com.tender.ui.base.MvpPresenter;
import com.tender.ui.base.MvpView;

public interface MainContract {

    public interface View extends MvpView {
    }

    public interface Presenter extends MvpPresenter {
    }
}
