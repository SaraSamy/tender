package com.tender.ui.main;

import com.tender.data.DataManager;
import com.tender.ui.base.BasePresenter;

public class MainPresenter extends BasePresenter implements MainContract.Presenter {
    public MainPresenter(DataManager dataManager) {
        super(dataManager);
    }
}
