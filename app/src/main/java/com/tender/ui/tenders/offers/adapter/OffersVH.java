package com.tender.ui.tenders.offers.adapter;


import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.tender.R;
import com.tender.model.Offer;
import com.tender.model.Tender;

import butterknife.BindView;
import butterknife.ButterKnife;


class OffersVH extends RecyclerView.ViewHolder {

    @BindView(R.id.nameTV)
    TextView nameTV;
    @BindView(R.id.price)
    TextView price;
    @BindView(R.id.points)
    TextView points;
    @BindView(R.id.pdf)
    TextView pdf;
    @BindView(R.id.img)
    ImageView img;

    OffersVH(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind(Offer offer) {
        nameTV.setText(offer.getAddedName());
        price.setText("Offer price "+offer.getPrice());

        if(offer.getAddedImg()!=null){
            Glide.with(itemView.getContext()).load(offer.getAddedImg()).into(img);
        }
        if(OffersAdapter.sort.equals("All"))
            points.setText(offer.getPoints()+" Points");
        else{
            for(int i=0;i<offer.getFeature().size();i++){
                if(offer.getFeature().get(i).getFeature().equals(OffersAdapter.sort))
                    points.setText(offer.getFeature().get(i).getPoints()+" Points");
            }
        }

    }
    public  void fPoints(Offer offer){
        points.setText(" Points");
    }
}
