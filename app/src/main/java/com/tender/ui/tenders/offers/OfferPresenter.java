package com.tender.ui.tenders.offers;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.tender.R;
import com.tender.data.DataManager;
import com.tender.model.Notification;
import com.tender.model.Offer;
import com.tender.model.PostTopic;
import com.tender.model.Tender;
import com.tender.ui.base.BaseLisener;
import com.tender.ui.base.BasePresenter;
import com.tender.ui.search.SearchContract;

import org.json.JSONException;
import org.json.JSONObject;

public class OfferPresenter<V extends OfferContract.View> extends BasePresenter<V>
        implements OfferContract.Presenter<V> , BaseLisener<Offer, String,String> {

    public OfferPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void onSuccess(Offer offer) {
        if (isViewAttached()) {
            getMvpView().hideLoading();
//            getMvpView().onSuccess(tender);
        }
    }

    @Override
    public void onSuccessData(Offer offer, String key) {
        if (isViewAttached()) {
            getMvpView().hideLoading();
            getMvpView().offers(offer);
        }
    }

    @Override
    public void onFail(String error) {
        if (isViewAttached()) {
            getMvpView().hideLoading();
            if (error.equals("no data")) {
                getMvpView().empty();
            } else
                getMvpView().showMessage(error);
        }
    }

    @Override
    public void getOffers(Tender tender) {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            getDataManager().getOffer("offers","tId",tender.getId(),this,Offer.class);
        } else {
            getMvpView().showMessage(R.string.error_no_iternet_connection);
        }
    }

    @Override
    public void closeTender(Tender tender) {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            getDataManager().updateData("tenders", tender.getId(), tender, closeTL);
        } else {
            getMvpView().showMessage(R.string.error_no_iternet_connection);
        }
    }

    private BaseLisener closeTL = new BaseLisener<Tender, String, String>() {
        @Override
        public void onSuccess(Tender data) {

        }

        @Override
        public void onSuccessData(Tender tender, String key) {
            if (isViewAttached()) {
                getMvpView().hideLoading();
                getMvpView().updated(tender);
            }
        }

        @Override
        public void onFail(String error) {
            if (isViewAttached()) {
                getMvpView().hideLoading();
                getMvpView().showMessage(error);
            }

        }
    };
}
