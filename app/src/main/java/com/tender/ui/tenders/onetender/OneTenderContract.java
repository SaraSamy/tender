package com.tender.ui.tenders.onetender;

import com.tender.model.Feature;
import com.tender.model.Offer;
import com.tender.model.Tender;
import com.tender.ui.base.MvpPresenter;
import com.tender.ui.base.MvpView;

public interface OneTenderContract {
     interface View extends MvpView {
        void feature(Feature feature);
         void empty();
         void updated(Tender tender);
         void offerAdded(Offer offer);
         void offered (Offer offer);
         void noOffer();
    }

    interface Presenter<V extends View> extends MvpPresenter<V> {
         void  getFeatures(String tenderId);
         void  closeTender(Tender tender);
         void  addOffer(Offer offer,Tender tender);
    }
}
