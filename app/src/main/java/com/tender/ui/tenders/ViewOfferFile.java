package com.tender.ui.tenders;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.tender.R;
import com.tender.model.Tender;
import com.tender.ui.base.BaseFragment;
import com.tender.ui.tenders.onetender.OneTenderFragment;

import org.jetbrains.annotations.NotNull;

import kotlin.jvm.internal.Intrinsics;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewOfferFile extends BaseFragment {


    public ViewOfferFile() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_view_offer_file, container, false);
    }


    public static ViewOfferFile newInstance(@NotNull String title,String file) {
        Intrinsics.checkParameterIsNotNull(title, "title");
        Bundle bundle = new Bundle();
        bundle.putString("file", file);
        ViewOfferFile fragment = new ViewOfferFile();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected void setUp(View view) {

    }
}
