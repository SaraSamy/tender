package com.tender.ui.tenders.onetender.adapter;

import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.adroitandroid.chipcloud.ChipCloud;
import com.adroitandroid.chipcloud.ChipListener;
import com.tender.R;
import com.tender.model.Feature;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.tender.ui.tenders.onetender.OneTenderFragment.meFeature;
import static com.tender.ui.tenders.onetender.OneTenderFragment.totalOffer;
import static com.tender.ui.tenders.onetender.adapter.TenderOfferAdapter.featureSelected;


public class TenderOfferVH extends RecyclerView.ViewHolder {

    @BindView(R.id.fTitle)
    TextView fTitle;
    @BindView(R.id.featuresChipCloud)
    ChipCloud featuresChipCloud;

    TenderOfferVH(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    ChipCloud chipCloudOffer;
    String[] featureArray;

    public void bind(String title, ArrayList<Feature> features) {
        fTitle.setText(title);
        featureArray = new String[features.size()];
        for (int x = 0; x < features.size(); x++) {
            featureArray[x] = features.get(x).getTitle();
        }
        chipCloudOffer = featuresChipCloud;
        new ChipCloud.Configure()
                .chipCloud(chipCloudOffer)
                .selectedColor(Color.parseColor("#FF2C2C"))
                .selectedFontColor(Color.parseColor("#ffffff"))
                .deselectedColor(Color.parseColor("#ff7a33"))
                .deselectedFontColor(Color.parseColor("#fddfff"))
                .selectTransitionMS(500)
                .deselectTransitionMS(250)
                .labels(featureArray)
                .mode(ChipCloud.Mode.SINGLE)
                .allCaps(false)
                .gravity(ChipCloud.Gravity.CENTER)
                .chipListener(new ChipListener() {
                    @Override
                    public void chipSelected(int index) {
                        for (int z=0;z<meFeature.size();z++){
                            if( meFeature.get(z).getFeature().equals(features.get(index).getFeature())){
                                totalOffer -= features.get(index).getPoints();
                                meFeature.remove(meFeature.get(z));
                            }
                        }
                            meFeature.add(features.get(index));
                            totalOffer += features.get(index).getPoints();
                            Log.e("oooooo", meFeature.toString());


                    }

                    @Override
                    public void chipDeselected(int index) {
                        meFeature.remove(features.get(index));
                        totalOffer -= features.get(index).getPoints();
                        Log.e("oooooo", meFeature.toString());
                    }
                })
                .build();
    }
}
