package com.tender.ui.tenders.add;

import android.net.Uri;

import androidx.annotation.NonNull;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;
import com.tender.R;
import com.tender.data.DataManager;
import com.tender.model.Feature;
import com.tender.model.Notification;
import com.tender.model.PostTopic;
import com.tender.model.PriceFeature;
import com.tender.model.Tender;
import com.tender.ui.base.BaseLisener;
import com.tender.ui.base.BasePresenter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

public class AddTenderPresenter<V extends AddTenderContract.View> extends BasePresenter<V>
        implements AddTenderContract.Presenter<V>, BaseLisener<Tender, String, String> {

    public AddTenderPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void onSuccess(Tender data) {

    }

    @Override
    public void onSuccessData(Tender data, String key) {
        if (isViewAttached()) {
            data.setId(key);
            PostTopic eventPostTopic = new PostTopic("/topics/tenders", new PostTopic.NotificationModel("New tender added", data.getTitle()), data);
            try {
                AndroidNetworking.post("https://fcm.googleapis.com/fcm/send")
                        .addJSONObjectBody(new JSONObject(new Gson().toJson(eventPostTopic, PostTopic.class)))
                        .addHeaders("Authorization", "key=AIzaSyAq4oV4_LBrmLzXA_DX7vyhJg1sApop7Fk")
                        .setPriority(Priority.MEDIUM)
                        .build().getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        getMvpView().hideLoading();
                        getDataManager().postNotification(new Notification(
                                "New tender added",
                                data.getTitle(),
                                "tenderAdded",
                                data.getId()
                        ));
                        getMvpView().tenderAdded(data);
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
            } catch (JSONException e) {
            }
        }
    }

    @Override
    public void onFail(String error) {
        if (isViewAttached()) {
            getMvpView().hideLoading();
            getMvpView().showMessage(error);
        }
    }

    @Override
    public void postTender(Tender tender, ArrayList<PriceFeature> priceFeatures, ArrayList<Feature> features) {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            getDataManager().uploadImage(Uri.parse(tender.getImg())).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    taskSnapshot.getMetadata()
                            .getReference()
                            .getDownloadUrl()
                            .addOnCompleteListener(new OnCompleteListener<Uri>() {
                                                       @Override
                                                       public void onComplete(@NonNull Task<Uri> task) {
                                                           tender.setImg(Objects.requireNonNull(task.getResult()).toString());
                                                           getDataManager().postData(
                                                                   "tenders",
                                                                   FirebaseDatabase.getInstance().getReference().push().getKey(),
                                                                   tender,
                                                                   AddTenderPresenter.this,
                                                                   priceFeatures,
                                                                   features);
                                                       }
                                                   }
                            );
                }
            });

        } else {
            getMvpView().showMessage(R.string.error_no_iternet_connection);
        }
    }
}
