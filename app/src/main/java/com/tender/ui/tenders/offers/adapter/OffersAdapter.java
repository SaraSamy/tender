package com.tender.ui.tenders.offers.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.tender.R;
import com.tender.model.Offer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class OffersAdapter extends RecyclerView.Adapter<OffersVH> {

    private ArrayList<Offer> offers;
    private OffersData offersData;

    public OffersAdapter(ArrayList<Offer> offers, OffersData offersData) {
        this.offers = offers;
        this.offersData = offersData;
    }

    @Override
    public OffersVH onCreateViewHolder(ViewGroup parent, int viewType) {
        return new OffersVH(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_offers, null));
    }
    private OffersVH holder;
    @Override
    public void onBindViewHolder(OffersVH holder, int position) {
        this.holder=holder;
        holder.bind(offers.get(position));
        holder.itemView.findViewById(R.id.accept).setOnClickListener(view -> {
            offersData.offerView(offers.get(position));
        });
        holder.itemView.findViewById(R.id.pdf).setOnClickListener(view -> {
            offersData.pdfView(offers.get(position));
        });
    }

    @Override
    public int getItemCount() {
        return offers.size();
    }

    public void addOffer(Offer offer) {
        if (offer != null) {
            offers.add(offer);
            Collections.sort(offers);
            notifyDataSetChanged();
        }
    }
    public  static String sort="";
    public void sort(String f){
        sort=f;
        if (f.equals("All"))
            Collections.sort(offers);
        else {
            Collections.sort(offers, new Comparator<Offer>() {
                @Override
                public int compare(Offer offer, Offer t1) {
                    return offer.compareTo(t1, f);
                }
            });
        }
        notifyDataSetChanged();
    }
    public ArrayList<Offer> getOffers() {
        return offers;
    }

    public interface OffersData {
        void offerView(Offer offer);
        void pdfView(Offer offer);
    }
}
