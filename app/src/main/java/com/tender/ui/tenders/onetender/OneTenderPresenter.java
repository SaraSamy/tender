package com.tender.ui.tenders.onetender;

import android.util.Log;

import androidx.annotation.NonNull;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.tender.R;
import com.tender.data.DataManager;
import com.tender.model.Feature;
import com.tender.model.Notification;
import com.tender.model.Offer;
import com.tender.model.PostTopic;
import com.tender.model.Tender;
import com.tender.ui.base.BaseLisener;
import com.tender.ui.base.BasePresenter;

import org.json.JSONException;
import org.json.JSONObject;

public class OneTenderPresenter<V extends OneTenderContract.View> extends BasePresenter<V>
        implements OneTenderContract.Presenter<V>, BaseLisener<Feature, String, String> {

    public OneTenderPresenter(DataManager dataManager) {
        super(dataManager);
    }
    Tender tender;

    @Override
    public void onSuccess(Feature data) {
        if (isViewAttached()) {
            getMvpView().hideLoading();
//            getMvpView().feature(data);
        }
    }

    private BaseLisener addOfferL = new BaseLisener<Offer, String, String>() {
        @Override
        public void onSuccess(Offer data) {
            if (isViewAttached()) {
                PostTopic eventPostTopic = new PostTopic("/topics/tenders", new PostTopic.NotificationModel("New offer added", "New offer added to tender "+tender.getTitle()),data );
                try {
                    AndroidNetworking.post("https://fcm.googleapis.com/fcm/send")
                            .addJSONObjectBody(new JSONObject(new Gson().toJson(eventPostTopic, PostTopic.class)))
                            .addHeaders("Authorization", "key=AIzaSyAq4oV4_LBrmLzXA_DX7vyhJg1sApop7Fk")
                            .setPriority(Priority.MEDIUM)
                            .build().getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("bla bla ","respo");
                        }
                        @Override
                        public void onError(ANError anError) {
                            Log.e("bla bla ",anError.getMessage());
                        }
                    });
                } catch (JSONException e) {
                    Log.e("bla bla ",e.getMessage());
                }
                getDataManager().postNotification(new Notification(
                        "New offer added",
                        "New offer added to tender "+tender.getTitle(),
                        "offerAdded",
                        data.gettId()
                ));
                getMvpView().hideLoading();
                getMvpView().offerAdded(data);
            }
        }

        @Override
        public void onSuccessData(Offer data, String key) {
            if (isViewAttached()) {

            }
        }

        @Override
        public void onFail(String error) {
            if (isViewAttached()) {
                getMvpView().hideLoading();
                getMvpView().showMessage(error);
            }
        }
    };
    @Override
    public void onSuccessData(Feature data, String key) {
        if (isViewAttached()) {
            getMvpView().hideLoading();
            Feature f=data;
            f.setId(key);
            getMvpView().feature(f);

        }
    }

    @Override
    public void onFail(String error) {
        if (isViewAttached()) {
            getMvpView().hideLoading();
            if (error.equals("no data")) {
                getMvpView().empty();
            } else
                getMvpView().showMessage(error);
        }
    }

    @Override
    public void getFeatures(String tenderId) {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            FirebaseDatabase.getInstance().getReference().child("offers").orderByChild("tId")
                    .equalTo(tenderId)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.getValue()!=null){
                                for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                                    if(childDataSnapshot.getValue(Offer.class).getAddedId().equals(FirebaseAuth.getInstance().getUid())){
                                        Offer offer = childDataSnapshot.getValue(Offer.class);
                                        offer.setOfferId(childDataSnapshot.getKey());
                                        getMvpView().offered(offer);
                                        break;
                                    }
                                    else
                                        getMvpView().noOffer();
//                                    Offer offer = childDataSnapshot.getValue(Offer.class);
//                                    getMvpView().offered(offer);
                                }
                            }
                            else{
                                getMvpView().noOffer();
                            }
                            getDataManager().getData("features", "tenderId", tenderId, OneTenderPresenter.this, Feature.class);

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
        } else {
            getMvpView().showMessage(R.string.error_no_iternet_connection);
        }
    }

    private BaseLisener closeTL = new BaseLisener<Tender, String, String>() {
        @Override
        public void onSuccess(Tender data) {

        }

        @Override
        public void onSuccessData(Tender tender, String key) {
            if (isViewAttached()) {
                getMvpView().hideLoading();
                getMvpView().updated(tender);
            }
        }

        @Override
        public void onFail(String error) {
            if (isViewAttached()) {
                getMvpView().hideLoading();
                getMvpView().showMessage(error);
            }

        }
    };

    @Override
    public void closeTender(Tender tender) {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            getDataManager().updateData("tenders", tender.getId(), tender, closeTL);
        } else {
            getMvpView().showMessage(R.string.error_no_iternet_connection);
        }
    }

    @Override
    public void addOffer(Offer offer,Tender tender) {
        if (getMvpView().isNetworkConnected()) {
            this.tender=tender;
            getDataManager().postOffer(offer,offer.getFeature(),addOfferL);
        } else {
            getMvpView().showMessage(R.string.error_no_iternet_connection);
        }
    }
}
