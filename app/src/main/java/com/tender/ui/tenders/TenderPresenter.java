package com.tender.ui.tenders;

import android.net.Uri;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.UploadTask;
import com.tender.R;
import com.tender.data.DataManager;
import com.tender.model.Feature;
import com.tender.model.PriceFeature;
import com.tender.model.Tender;
import com.tender.ui.base.BaseLisener;
import com.tender.ui.base.BasePresenter;
import com.tender.ui.tenders.add.AddTenderContract;

import java.util.ArrayList;
import java.util.Objects;

public class TenderPresenter<V extends TenderContract.View> extends BasePresenter<V>
        implements TenderContract.Presenter<V> , BaseLisener<Tender, String,String> {

    public TenderPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void onSuccess(Tender data) {

    }

    @Override
    public void onSuccessData(Tender data, String key) {
        if (isViewAttached()) {
            data.setId(key);
            getMvpView().hideLoading();
            getMvpView().tender(data);
        }
    }

    @Override
    public void onFail(String error) {
        if (isViewAttached()) {
            getMvpView().hideLoading();
            if (error.equals("no data")) {
                getMvpView().empty();
            } else
                getMvpView().showMessage(error);
        }
    }

    @Override
    public void getTender(String eq) {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            getDataManager().getData("tenders", "type", eq, this, Tender.class);
        } else {
            getMvpView().showMessage(R.string.error_no_iternet_connection);
        }
    }

}
