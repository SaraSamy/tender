package com.tender.ui.tenders.offers;

import com.tender.model.Offer;
import com.tender.model.Tender;
import com.tender.ui.base.MvpPresenter;
import com.tender.ui.base.MvpView;

public interface OfferContract {
     interface View extends MvpView {
         void empty();
         void  offers(Offer offer);
         void updated(Tender tender);
    }

    interface Presenter<V extends View> extends MvpPresenter<V> {
         void  getOffers(Tender tender);
        void  closeTender(Tender tender);
    }
}
