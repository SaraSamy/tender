package com.tender.ui.tenders.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.tender.R;
import com.tender.model.Tender;
import butterknife.BindView;
import butterknife.ButterKnife;


class TenderVH extends RecyclerView.ViewHolder {

    @BindView(R.id.nameTV)
    TextView nameTV;
    @BindView(R.id.discTV)
    TextView discTV;
    @BindView(R.id.forTV)
    TextView forTV;
    @BindView(R.id.tenderIV)
    ImageView tenderIV;

    TenderVH(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind(Tender tender) {
        nameTV.setText(tender.getTitle());
        discTV.setText(tender.getDisc());
        if(tender.getImg()!=null){
            Glide.with(itemView.getContext()).load(tender.getImg()).into(tenderIV);
        }
        if(tender.getType().equals("closed")){
            if(tender.getClosedForName()!=null)
                forTV.setText("Closed for "+tender.getClosedForName());
            else
                forTV.setText("Closed by admin");
        }
    }
}
