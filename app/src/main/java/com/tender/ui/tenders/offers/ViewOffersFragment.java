package com.tender.ui.tenders.offers;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adroitandroid.chipcloud.ChipCloud;
import com.adroitandroid.chipcloud.ChipListener;
import com.tender.R;
import com.tender.data.AppDataManager;
import com.tender.model.Feature;
import com.tender.model.Offer;
import com.tender.model.Tender;
import com.tender.ui.base.BaseFragment;
import com.tender.ui.tenders.offers.adapter.OffersAdapter;
import com.tender.ui.tenders.onetender.OneTenderFragment;
import com.tender.utils.ViewUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import gun0912.tedbottompicker.GridSpacingItemDecoration;
import kotlin.jvm.internal.Intrinsics;

public class ViewOffersFragment extends BaseFragment implements OfferContract.View,OffersAdapter.OffersData {

    @BindView(R.id.offersRV)
    RecyclerView offersRV;
    @BindView(R.id.NoDataIV)
    ImageView NoDataIV;
    @BindView(R.id.NoDataTV)
    TextView NoDataTV;
    OfferPresenter presenter;
    OffersAdapter adapter;
    private ArrayList<Offer> offers =new ArrayList<>();
    Map<String, ArrayList<Feature>> tenderFeatures = new HashMap<String, ArrayList<Feature>>();
    Tender tender;
    public ViewOffersFragment() {
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        tenderFeatures = new HashMap<String, ArrayList<Feature>>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_offers, container, false);
        ButterKnife.bind(this, view);
        return view;
    }
    @BindView(R.id.filterChipCloud)
    ChipCloud filterChipCloud;
    @Override
    protected void setUp(View view) {
        Bundle bundle  =getArguments();
        tender= (Tender) bundle.getSerializable("tender");
        tenderFeatures = new HashMap<String, ArrayList<Feature>>();
        tenderFeatures =OneTenderFragment.response;
        Log.e("rrrr",tenderFeatures.keySet().toString());
        if(tender!=null){
            offers =new ArrayList<>();
            adapter = new OffersAdapter(offers,this);
            presenter =new OfferPresenter(new AppDataManager());
            presenter.onAttach(this);
            presenter.getOffers(tender);
            setUpRecyclers(offersRV);
        }

        String[] fArr=new  String[tenderFeatures.size()+1];
        fArr[0]="All";
        int i=1;
        for (String key : tenderFeatures.keySet()){
            fArr[i]=key;
            for (int x=0;x<tenderFeatures.get(key).size();x++){
                Log.e("aaaa", String.valueOf(tenderFeatures.get(key).get(x).getPoints()));
            }
            i++;
        }
        ChipCloud chipCloud = filterChipCloud;
        new ChipCloud.Configure()
                .chipCloud(chipCloud)
                .selectedColor(Color.parseColor("#FF2C2C"))
                .selectedFontColor(Color.parseColor("#ffffff"))
                .deselectedColor(Color.parseColor("#fffffd"))
                .deselectedFontColor(Color.parseColor("#FF2C2C"))
                .selectTransitionMS(500)
                .deselectTransitionMS(250)
                .labels( fArr)
                .mode(ChipCloud.Mode.SINGLE)
                .allCaps(false)
                .gravity(ChipCloud.Gravity.CENTER)
                .textSize(getResources().getDimensionPixelSize(R.dimen.default_textsize))
                .chipListener(new ChipListener() {
                    @Override
                    public void chipSelected(int index) {
                        adapter.sort(fArr[index]);
                    }
                    @Override
                    public void chipDeselected(int index) {

                    }
                })
                .build();chipCloud.setSelectedChip(0);
    }

    public static ViewOffersFragment newInstance(@NotNull String title, Tender tender ) {
        Intrinsics.checkParameterIsNotNull(title, "title");
        Bundle bundle = new Bundle();
        bundle.putSerializable("tender", tender);
        ViewOffersFragment fragment = new ViewOffersFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    private void setUpRecyclers(@NotNull RecyclerView recyclerView) {
        GridLayoutManager girdLayoutManager = new GridLayoutManager(this.getContext(), 1);
        recyclerView.setLayoutManager(girdLayoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration((new GridSpacingItemDecoration(1, ViewUtils.dpToPx(8.0F), true)));
    }

    @Override
    public void empty() {
        View view = getView();
        if(view!=null){
            view.findViewById(R.id.NoDataTV).setVisibility(View.VISIBLE);
            view.findViewById(R.id.NoDataIV).setVisibility(View.VISIBLE);
            view.findViewById(R.id.offersRV).setVisibility(View.GONE);
        }
    }

    @Override
    public void offers(Offer offer) {
        View view = getView();
        if(view!=null){
            view.findViewById(R.id.NoDataTV).setVisibility(View.GONE);
            view.findViewById(R.id.NoDataIV).setVisibility(View.GONE);
            view.findViewById(R.id.offersRV).setVisibility(View.VISIBLE);
        }
        adapter.addOffer(offer);
    }

    @Override
    public void offerView(Offer offer) {
        tender.setType("closed");
        tender.setClosedFor(offer.getAddedId());
        tender.setClosedForName(offer.getAddedName());
        presenter.closeTender(tender);
    }

    @Override
    public void pdfView(Offer offer) {
        getBaseActivity().replaceFragmentToActivity(PdfViewFragment.newInstance("", offer.getFile()), true);
    }

    @Override
    public void updated(Tender tender) {
        Toast.makeText(getContext(),"Tender closed",Toast.LENGTH_SHORT).show();
        getBaseActivity().getSupportFragmentManager().popBackStack();
    }
}
