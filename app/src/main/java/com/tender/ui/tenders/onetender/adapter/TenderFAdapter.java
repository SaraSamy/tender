package com.tender.ui.tenders.onetender.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.tender.R;
import com.tender.model.Feature;

import java.util.ArrayList;
import java.util.Map;

public class TenderFAdapter extends RecyclerView.Adapter<TenderFVH> {

    private Map<String, ArrayList<Feature>> f;

    public TenderFAdapter(Map<String, ArrayList<Feature>>feat) {
        this.f = feat;
    }

    @Override
    public TenderFVH onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TenderFVH(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_feat, null));
    }

    @Override
    public void onBindViewHolder(TenderFVH holder, int position) {
        String key = ((String) f.keySet().toArray()[position]);
        holder.bind(key, f.get(key));
    }

    @Override
    public int getItemCount() {
        return f.size();
    }

    public void addFeaturers(Map<String, ArrayList<Feature>> feat) {
        if (f != null) {
                f=feat;
                notifyDataSetChanged();
        }
    }

    public Map<String, ArrayList<Feature>> getFeatures() {
        return f;
    }

    public interface FeatureClick{
        void featureClick(Feature feature);
    }
}
