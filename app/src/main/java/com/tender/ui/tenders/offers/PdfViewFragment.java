package com.tender.ui.tenders.offers;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.github.barteksc.pdfviewer.PDFView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.shockwave.pdfium.PdfDocument;
import com.tender.R;
import com.tender.ui.base.BaseFragment;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class PdfViewFragment extends BaseFragment {
    private static final String ARG_PARAM1 = "param1";

    private String mParam1;

    public PdfViewFragment() {
        // Required empty public constructor
    }

    public static PdfViewFragment newInstance(@NotNull String title, String param1) {
        PdfViewFragment fragment = new PdfViewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pdf_view, container, false);
    }


    @Override
    protected void setUp(View view) {
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://docs.google.com/gview?embedded=true&url=" +mParam1));
        startActivity(i);
////        String url="";
//        PDFView pdfView = (PDFView) view.findViewById(R.id.pdfView);
//        pdfView.fromUri(Uri.parse("http://docs.google.com/gview?embedded=true&url=" +mParam1+".pdf"));
//        mywebview.getSettings().setJavaScriptEnabled(true);
//        try {
//             url= URLEncoder.encode(mParam1,"UTF-8");
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//        pdfView.fromUri(Uri.parse("http://dl.dropboxusercontent.com/s/2v42h36tdcd6ii8/portrait.pdf"))
//                .defaultPage(currentPage)
//                .swipeHorizontal(true).load();
//        mywebview.loadUrl( "http://docs.google.com/gview?embedded=true&url=" +mParam1+".pdf");
//

//        FirebaseStorage storage = FirebaseStorage.getInstance();
//// Create a storage reference from our app
//        StorageReference storageRef = storage.getReference();
//
//// Create a reference with an initial file path and name
//        StorageReference pathReference = storageRef.child("images/stars.jpg");
//
//// Create a reference to a file from a Google Cloud Storage URI
//        StorageReference gsReference = storage.getReferenceFromUrl("gs://bucket/images/stars.jpg");
//
//// Create a reference from an HTTPS URL
//// Note that in the URL, characters are URL escaped!
//        StorageReference httpsReference = storage.getReferenceFromUrl(mParam1);
    }
}
