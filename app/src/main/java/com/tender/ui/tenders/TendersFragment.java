package com.tender.ui.tenders;


import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.FirebaseDatabase;
import com.tender.R;
import com.tender.data.AppDataManager;
import com.tender.model.Feature;
import com.tender.model.Tender;
import com.tender.ui.base.BaseFragment;
import com.tender.ui.search.SearchFragment;
import com.tender.ui.tenders.adapter.TenderAdapter;
import com.tender.ui.tenders.add.AddTenderFragment;
import com.tender.ui.tenders.onetender.OneTenderFragment;
import com.tender.utils.CommonUtils;
import com.tender.utils.ViewUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import gun0912.tedbottompicker.GridSpacingItemDecoration;
import kotlin.jvm.internal.Intrinsics;

import static android.content.Context.MODE_PRIVATE;

public class TendersFragment extends BaseFragment implements TenderContract.View,TenderAdapter.TenderData {

    @BindView(R.id.tendersRV)
    RecyclerView tendersRV;
    @BindView(R.id.searchView)
    CardView searchView;
    @BindView(R.id.addFab)
    FloatingActionButton addFab;
    TenderPresenter presenter;
    TenderAdapter adapter;
    private ArrayList<Tender> tenders =new ArrayList<>();

    public TendersFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tenders, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    public static TendersFragment newInstance(@NotNull String title) {
        Intrinsics.checkParameterIsNotNull(title, "title");
        return new TendersFragment();
    }

    @Override
    protected void setUp(View view) {
        SharedPreferences prefs = Objects.requireNonNull(getActivity()).getSharedPreferences("Tender", MODE_PRIVATE);
        String type = prefs.getString("type", "type");
        if (type.equals("admin")) {
            addFab.setVisibility(View.VISIBLE);
        }
        addFab.setOnClickListener(view1 -> {
            getBaseActivity().replaceFragmentToActivity(AddTenderFragment.newInstance(""),true);
        });
        tenders =new ArrayList<>();
        adapter = new TenderAdapter(tenders,this);
        presenter =new TenderPresenter(new AppDataManager());
        presenter.onAttach(this);
        presenter.getTender("open");
        setUpRecyclers(tendersRV);
        searchView.setOnClickListener(view1 -> {
            getBaseActivity().replaceFragmentToActivity(SearchFragment.newInstance(""), true);
        });
    }

    private void setUpRecyclers(@NotNull RecyclerView recyclerView) {
        GridLayoutManager girdLayoutManager = new GridLayoutManager(this.getContext(), 1);
        recyclerView.setLayoutManager(girdLayoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration((new GridSpacingItemDecoration(1, ViewUtils.dpToPx(8.0F), true)));
    }

    static void f(Feature feature){

    }
    @Override
    public void tender(Tender tender) {
        View view = getView();
        if (view != null) {
            view.findViewById(R.id.NoDataTV).setVisibility(View.GONE);
            view.findViewById(R.id.NoDataIV).setVisibility(View.GONE);
            view.findViewById(R.id.tendersRV).setVisibility(View.VISIBLE);
        }
        if (!tender.getType().equals("closed")) {
            adapter.addTenderr(tender);
        }
    }

    @Override
    public void empty() {
        View view = getView();
        if(view!=null){
            view.findViewById(R.id.NoDataTV).setVisibility(View.VISIBLE);
            view.findViewById(R.id.NoDataIV).setVisibility(View.VISIBLE);
            view.findViewById(R.id.tendersRV).setVisibility(View.GONE);
        }
    }


    @Override
    public void tenderView(Tender tender) {
        getBaseActivity().replaceFragmentToActivity(OneTenderFragment.newInstance("",tender), true);
    }
}
