package com.tender.ui.tenders.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.tender.R;
import com.tender.model.Tender;

import java.util.ArrayList;

public class TenderAdapter extends RecyclerView.Adapter<TenderVH> {

    private ArrayList<Tender> tenders;
    private TenderData tenderData;

    public TenderAdapter(ArrayList<Tender> photographers, TenderData tenderData) {
        this.tenders = photographers;
        this.tenderData=tenderData;
    }

    @Override
    public TenderVH onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TenderVH(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_tender, null));
    }

    @Override
    public void onBindViewHolder(TenderVH holder, int position) {
        holder.bind(tenders.get(position));
        holder.itemView.setOnClickListener(view -> {
            tenderData.tenderView(tenders.get(position));
        });
    }

    @Override
    public int getItemCount() {
        return tenders.size();
    }

    public void addTenderr(Tender tender) {
        if (tender != null) {
                tenders.add(tender);
                notifyDataSetChanged();
        }
    }
    public void addClosedTenderr(Tender tender) {
        if (tender != null) {
                tenders.add(tender);
                notifyDataSetChanged();
        }
    }
    public void updateItem(Tender user) {
        for (int i = 0; i < tenders.size(); i++) {
            if (tenders.get(i).equals(user)) {
                tenders.set(i, user);
                notifyDataSetChanged();
                return;
            }
        }
    }

    public void deleteItem(Tender tender){
        for (int i = 0; i < tenders.size(); i++) {
            if (tenders.get(i).equals(tender)) {
                tenders.remove(i);
                notifyDataSetChanged();
                return;
            }
        }
    }

    public void addTenders(ArrayList<Tender> mTenders) {
        if (mTenders != null) {
            this.tenders.addAll(mTenders);
            notifyDataSetChanged();
        }
    }

    public ArrayList<Tender> getTender() {
        return tenders;
    }

    public interface TenderData {
        void tenderView(Tender tender);
    }
}
