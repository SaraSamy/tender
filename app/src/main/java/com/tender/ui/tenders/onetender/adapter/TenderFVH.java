package com.tender.ui.tenders.onetender.adapter;

import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.adroitandroid.chipcloud.ChipCloud;
import com.adroitandroid.chipcloud.ChipListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.FirebaseDatabase;
import com.marcoscg.dialogsheet.DialogSheet;
import com.tender.R;
import com.tender.model.Feature;
import com.tender.ui.tenders.onetender.OneTenderFragment;
import com.tender.utils.CommonUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;



class TenderFVH extends RecyclerView.ViewHolder {

    @BindView(R.id.fTitle)
    TextView fTitle;
    @BindView(R.id.featuresChipCloud)
    ChipCloud featuresChipCloud;

    TenderFVH(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind(String title, ArrayList<Feature> features) {
        fTitle.setText(title);
        String[] featureArray = new String[features.size()];
        for (int x = 0; x < features.size(); x++) {
            featureArray[x] = features.get(x).getTitle();
        }
//        features.toArray(featureArray);
        ChipCloud chipCloud = featuresChipCloud;
        new ChipCloud.Configure()
                .chipCloud(chipCloud)
                .selectedColor(Color.parseColor("#ff7a33"))
                .selectedFontColor(Color.parseColor("#ffffff"))
                .deselectedColor(Color.parseColor("#ff7a33"))
                .deselectedFontColor(Color.parseColor("#fddfff"))
                .selectTransitionMS(500)
                .deselectTransitionMS(250)
                .labels(featureArray)
        .mode(ChipCloud.Mode.SINGLE)
                .allCaps(false)
                .gravity(ChipCloud.Gravity.CENTER)
                .chipListener(new ChipListener() {
                    @Override
                    public void chipSelected(int index) {
                        SharedPreferences prefs = Objects.requireNonNull(itemView.getContext()).getSharedPreferences("Tender", MODE_PRIVATE);
                        String type = prefs.getString("type", "type");
                        if (type.equals("admin")) {
                            DialogSheet dialogSheet = new DialogSheet(Objects.requireNonNull(itemView.getContext()));
                            dialogSheet.setTitle("update feature points")
                                    .setColoredNavigationBar(true)
                                    .setView(R.layout.feature_edit)
                                    .setTitleTextSize(20)
                                    .setButtonsColorRes(R.color.colorAccent)
                                    .show();
                            View inflatedView = dialogSheet.getInflatedView();
                            Button addFeature = (Button) inflatedView.findViewById(R.id.addFeature);
                            EditText pointsET = (EditText) inflatedView.findViewById(R.id.pointsET);
                            pointsET.setText(String.valueOf(features.get(index).getPoints()));
                            EditText value = (EditText) inflatedView.findViewById(R.id.value);
                            value.setText(features.get(index).getTitle());
                            addFeature.setOnClickListener(view -> {
                                if (value.getText().toString().isEmpty()) {
                                    value.setError("value is required");
                                    value.requestFocus();
                                }
                                if (pointsET.getText().toString().isEmpty()) {
                                    pointsET.setError("points is required");
                                    pointsET.requestFocus();
                                } else {

                                    features.get(index).setTitle(value.getText().toString());
                                    features.get(index).setPoints(Integer.parseInt(pointsET.getText().toString()));
                                    AlertDialog mProgressDialog;
                                    mProgressDialog = CommonUtils.showLoadingDialog(itemView.getContext());
                                    OneTenderFragment.response.clear();
                                    FirebaseDatabase.getInstance().getReference().child("features").child(features.get(index).getId())
                                            .setValue(features.get(index)).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            mProgressDialog.cancel();
                                            Toast.makeText(itemView.getContext(), "updated", Toast.LENGTH_LONG).show();
                                        }
                                    });
                                    dialogSheet.dismiss();

                                }
                            });
                        }
                    }

                    @Override
                    public void chipDeselected(int index) {

                    }
                })
                .build();

    }
}
