package com.tender.ui.tenders.onetender.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.tender.R;
import com.tender.model.Feature;

import java.util.ArrayList;
import java.util.Map;

import static com.tender.ui.tenders.onetender.OneTenderFragment.meFeature;

public class TenderOfferAdapter extends RecyclerView.Adapter<TenderOfferVH> {

    private Map<String, ArrayList<Feature>> f;

    public static  ArrayList<Feature> featureSelected=null;

    public TenderOfferAdapter(Map<String, ArrayList<Feature>>feat) {
        this.f = feat;
    }

    @Override
    public TenderOfferVH onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TenderOfferVH(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_feat_offer, null));
    }

    @Override
    public void onBindViewHolder(TenderOfferVH holder, int position) {
        String key = ((String) f.keySet().toArray()[position]);
        holder.bind(key, f.get(key));
        if(featureSelected!=null){
            for(int o=0;o<featureSelected.size();o++){
                for(int i=0;i<holder.featureArray.length;i++){
                    if(holder.featureArray[i].equals(featureSelected.get(o).getTitle())){
                        holder.featuresChipCloud.setSelectedChip(i);
                    }
                }
            }


        }
    }

    @Override
    public int getItemCount() {
        return f.size();
    }

    public void addFeaturers(Map<String, ArrayList<Feature>> feat) {
        if (f != null) {
                f=feat;
                notifyDataSetChanged();
        }
    }

    public Map<String, ArrayList<Feature>> getFeatures() {
        return f;
    }

    public interface FeatureClick{
        void featureClick(Feature feature);
    }
}
