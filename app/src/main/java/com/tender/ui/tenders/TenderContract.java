package com.tender.ui.tenders;

import com.tender.model.Feature;
import com.tender.model.PriceFeature;
import com.tender.model.Tender;
import com.tender.ui.base.MvpPresenter;
import com.tender.ui.base.MvpView;

import java.util.ArrayList;

public interface TenderContract {
     interface View extends MvpView {
        void tender(Tender tender);
         void empty();
    }

    interface Presenter<V extends View> extends MvpPresenter<V> {
         void getTender(String eq);
//         void editF(Feature feature);
    }
}
