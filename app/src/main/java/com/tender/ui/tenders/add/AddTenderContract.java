package com.tender.ui.tenders.add;

import com.tender.model.Feature;
import com.tender.model.PriceFeature;
import com.tender.model.Tender;
import com.tender.model.User;
import com.tender.ui.base.MvpPresenter;
import com.tender.ui.base.MvpView;

import java.util.ArrayList;

public interface AddTenderContract {
     interface View extends MvpView {
        //void onSuccess();
         void tenderAdded(Tender tender);
    }

    interface Presenter<V extends View> extends MvpPresenter<V> {
         void postTender(Tender tender,ArrayList<PriceFeature> PriceFeatures,ArrayList<Feature> features);
    }
}
