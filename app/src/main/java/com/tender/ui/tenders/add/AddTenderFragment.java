package com.tender.ui.tenders.add;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.adroitandroid.chipcloud.ChipCloud;
import com.adroitandroid.chipcloud.ChipListener;
import com.bumptech.glide.Glide;
import com.marcoscg.dialogsheet.DialogSheet;
import com.tender.R;
import com.tender.data.AppDataManager;
import com.tender.model.Feature;
import com.tender.model.PriceFeature;
import com.tender.model.Tender;
import com.tender.ui.base.BaseFragment;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import gun0912.tedbottompicker.TedRxBottomPicker;
import kotlin.jvm.internal.Intrinsics;

import static androidx.core.content.ContextCompat.checkSelfPermission;

public class AddTenderFragment extends BaseFragment implements AddTenderContract.View {


    @BindView(R.id.tenderIV)
    ImageView tenderIV;
    @BindView(R.id.imageChooser)
    ImageView imageChooser;
    @BindView(R.id.titleET)
    EditText titleET;
    @BindView(R.id.discET)
    EditText discET;
    @BindView(R.id.addFeatureIV)
    ImageView addFeatureIV;
    @BindView(R.id.featuresChipCloud)
    ChipCloud featuresChipCloud;
    @BindView(R.id.addB)
    Button addB;
    @BindView(R.id.priceFeatureIV)
    ImageView priceFeatureIV;
    @BindView(R.id.priceChipCloud)
    ChipCloud priceChipCloud;

    public AddTenderFragment() {
    }

    ArrayList<String> featureList = new ArrayList<>();
    ArrayList<String> featuresL = new ArrayList<>();
    ArrayList<String> priceList = new ArrayList<>();

    ArrayList<PriceFeature> priceFeatures = new ArrayList<>();
    ArrayList<Feature> features = new ArrayList<>();
    private static final int MY_PERMISSIONS_REQUEST = 100;
    Tender tender;
    int totalPoints = 0;
    String image = "";
    AddTenderPresenter presenter = new AddTenderPresenter(new AppDataManager());


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_tender, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @SuppressLint("CheckResult")
    @Override
    protected void setUp(View view) {
        presenter.onAttach(this);
        addFeatureIV.setOnClickListener(view1 -> {
            addFeatureBottomSheet();
        });
        priceFeatureIV.setOnClickListener(view1 -> {
            addPriceBottomSheet();
        });
        imageChooser.setOnClickListener(view1 -> {
            getImage();
        });
        addB.setOnClickListener(view1 -> {
            if (image.equals("")) {
                Toast.makeText(getContext(), "Image must added", Toast.LENGTH_SHORT).show();
                return;
            }
            if (titleET.getText().toString().isEmpty()) {
                titleET.setError("Title is required");
                titleET.requestFocus();
                return;
            }
            if (discET.getText().toString().isEmpty()) {
                discET.setError("Description is required");
                discET.requestFocus();
                return;
            } else {
                tender = new Tender(
                        null,
                        titleET.getText().toString(),
                        discET.getText().toString(),
                        totalPoints,
                        image,
                        "open",
                        null
                );
                if (features.size() != 0 && priceFeatures.size() == 0) {
                    presenter.postTender(tender, null, features);
                }
                if (priceFeatures.size() != 0 && features.size() == 0) {
                    presenter.postTender(tender, priceFeatures, null);
                }
                if (priceFeatures.size() != 0 && features.size() != 0) {
                    presenter.postTender(tender, priceFeatures, features);
                }
                if (priceFeatures.size() == 0 && features.size() == 0) {
                    presenter.postTender(tender, null, null);
                }
            }
        });
    }

    public static AddTenderFragment newInstance(@NotNull String title) {
        Intrinsics.checkParameterIsNotNull(title, "title");
        return new AddTenderFragment();
    }

    private void addFeatureBottomSheet() {
        DialogSheet dialogSheet = new DialogSheet(Objects.requireNonNull(getContext()));
        dialogSheet.setTitle("Add feature points")
                .setColoredNavigationBar(true)
                .setView(R.layout.add_feature)
                .setTitleTextSize(20)
                .setButtonsColorRes(R.color.colorAccent)
                .show();
        View inflatedView = dialogSheet.getInflatedView();
        Button addFeature = (Button) inflatedView.findViewById(R.id.addFeature);
        EditText featureET = inflatedView.findViewById(R.id.featureET);
        ImageView valueIV =inflatedView.findViewById(R.id.valueIV);
        ChipCloud featuresvalueChipCloud=inflatedView.findViewById(R.id.featuresvalueChipCloud);
        final String[][] featureArray = {new String[featureList.size()]};
        valueIV.setOnClickListener(view -> {
            if (featureET.getText().toString().isEmpty()) {
                featureET.setError("Feature title is required");
                featureET.requestFocus();
                return;
            }else {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
                LayoutInflater inflater = this.getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.feature_alert, null);
                alertDialog.setView(dialogView);
                EditText pointsET = (EditText) dialogView.findViewById(R.id.pointsET);
                EditText value = (EditText) dialogView.findViewById(R.id.value);
                alertDialog.setPositiveButton("Done",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (value.getText().toString().isEmpty()) {
                                    value.setError("value is required");
                                    value.requestFocus();
                                }
                                if (pointsET.getText().toString().isEmpty()) {
                                    pointsET.setError("points is required");
                                    pointsET.requestFocus();
                                } else {
                                    features.add(new Feature(
                                            value.getText().toString(),
                                            featureET.getText().toString(),
                                            Integer.parseInt(pointsET.getText().toString()),
                                            "",null));
                                    featureList.add(value.getText().toString() + " = " + pointsET.getText().toString() + " point");
                                    featureArray[0] = new String[featureList.size()];
                                    featureList.toArray(featureArray[0]);
                                    totalPoints += Integer.parseInt(pointsET.getText().toString());
                                    ChipCloud chipCloud = featuresvalueChipCloud;
                                    new ChipCloud.Configure()
                                            .chipCloud(chipCloud)
                                            .selectedColor(Color.parseColor("#FF2C2C"))
                                            .selectedFontColor(Color.parseColor("#ffffff"))
                                            .deselectedColor(Color.parseColor("#ff7a33"))
                                            .deselectedFontColor(Color.parseColor("#fddfff"))
                                            .selectTransitionMS(500)
                                            .deselectTransitionMS(250)
                                            .labels(featureArray[0])
                                            .mode(ChipCloud.Mode.NONE)
                                            .allCaps(false)
                                            .gravity(ChipCloud.Gravity.CENTER)
                                            .textSize(getResources().getDimensionPixelSize(R.dimen.default_textsize))
                                            .chipListener(new ChipListener() {
                                                @Override
                                                public void chipSelected(int index) {

                                                }

                                                @Override
                                                public void chipDeselected(int index) {

                                                }
                                            })
                                            .build();

                                }

                            }
                        });

                alertDialog.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                alertDialog.show();
            }
        });
        addFeature.setOnClickListener(view -> {
            if (featureET.getText().toString().isEmpty()) {
                featureET.setError("Feature title is required");
                featureET.requestFocus();
                return;
            } if(featureArray[0].length==0){
                dialogSheet.dismiss();
                Toast.makeText(getContext(),"Feature values are required",Toast.LENGTH_LONG).show();
                return;
            }
            else {
                featuresL.add(featureET.getText().toString());
                String[] featureA = new String[featuresL.size()];
                featuresL.toArray(featureA);
                ChipCloud chipCloud = featuresChipCloud;
                new ChipCloud.Configure()
                        .chipCloud(chipCloud)
                        .selectedColor(Color.parseColor("#FF2C2C"))
                        .selectedFontColor(Color.parseColor("#ffffff"))
                        .deselectedColor(Color.parseColor("#ff7a33"))
                        .deselectedFontColor(Color.parseColor("#fddfff"))
                        .selectTransitionMS(500)
                        .deselectTransitionMS(250)
                        .labels(featureA)
                        .mode(ChipCloud.Mode.NONE)
                        .allCaps(false)
                        .gravity(ChipCloud.Gravity.CENTER)
                        .textSize(getResources().getDimensionPixelSize(R.dimen.default_textsize))
                        .chipListener(new ChipListener() {
                            @Override
                            public void chipSelected(int index) {

                            }

                            @Override
                            public void chipDeselected(int index) {

                            }
                        })
                        .build();
                featureList.clear();
                dialogSheet.dismiss();
            }
        });
    }

    private void addPriceBottomSheet() {
        DialogSheet dialogSheet = new DialogSheet(Objects.requireNonNull(getContext()));
        dialogSheet.setTitle("Add feature points")
                .setColoredNavigationBar(true)
                .setView(R.layout.price_feature)
                .setTitleTextSize(20)
                .setButtonsColorRes(R.color.colorAccent)
                .show();
        View inflatedView = dialogSheet.getInflatedView();
        Button addPriceFeature = (Button) inflatedView.findViewById(R.id.addPriceFeature);
        EditText pointsET = inflatedView.findViewById(R.id.pointsET);
        EditText fromEV = inflatedView.findViewById(R.id.fromEV);
        EditText toET = inflatedView.findViewById(R.id.toET);
        addPriceFeature.setOnClickListener(view -> {
            if (fromEV.getText().toString().isEmpty()) {
                fromEV.setError("required");
                fromEV.requestFocus();
                return;
            }
            if (toET.getText().toString().isEmpty()) {
                toET.setError("required");
                toET.requestFocus();
                return;
            }
            if (pointsET.getText().toString().isEmpty()) {
                pointsET.setError("points is required");
                pointsET.requestFocus();
                return;
            } else {
                priceFeatures.add(new PriceFeature(
                        Integer.parseInt(pointsET.getText().toString()),
                        Integer.parseInt(fromEV.getText().toString()),
                        Integer.parseInt(toET.getText().toString()),
                        "",null));
                priceList.add("From " + fromEV.getText().toString() + " to " + toET.getText().toString() + " = " + pointsET.getText().toString() + " point");
                String[] priceArray = new String[priceList.size()];
                priceList.toArray(priceArray);
                totalPoints += Integer.parseInt(pointsET.getText().toString());
                ChipCloud chipCloud = priceChipCloud;
                new ChipCloud.Configure()
                        .chipCloud(chipCloud)
                        .selectedColor(Color.parseColor("#FF2C2C"))
                        .selectedFontColor(Color.parseColor("#ffffff"))
                        .deselectedColor(Color.parseColor("#ff7a33"))
                        .deselectedFontColor(Color.parseColor("#fddfff"))
                        .selectTransitionMS(500)
                        .deselectTransitionMS(250)
                        .labels(priceArray)
                        .mode(ChipCloud.Mode.NONE)
                        .allCaps(false)
                        .gravity(ChipCloud.Gravity.CENTER)
                        .textSize(getResources().getDimensionPixelSize(R.dimen.default_textsize))
                        .chipListener(new ChipListener() {
                            @Override
                            public void chipSelected(int index) {

                            }

                            @Override
                            public void chipDeselected(int index) {

                            }
                        })
                        .build();
                dialogSheet.dismiss();
            }
        });
    }

    private void getImage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED &&

                    checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED &&

                    checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {

                requestPermissions(new String[]{Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST);
                return;
            }
        }
        TedRxBottomPicker.with(getActivity())
                .show()
                .subscribe(uri -> {
                            Glide.with(this)
                                    .load(uri)
                                    .into(tenderIV);
                            image = uri.toString();
                        },
                        throwable -> {
                        });
    }

    @Override
    public void tenderAdded(Tender tender) {
        Toast.makeText(getContext(),"Tender added",Toast.LENGTH_SHORT).show();
        getBaseActivity().getSupportFragmentManager().popBackStack();
    }
}
