package com.tender.ui.tenders.onetender;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.UploadTask;
import com.marcoscg.dialogsheet.DialogSheet;
import com.tender.R;
import com.tender.data.AppDataManager;
import com.tender.model.Feature;
import com.tender.model.Offer;
import com.tender.model.Tender;
import com.tender.ui.base.BaseFragment;
import com.tender.ui.tenders.ViewOfferFile;
import com.tender.ui.tenders.offers.ViewOffersFragment;
import com.tender.ui.tenders.onetender.adapter.TenderFAdapter;
import com.tender.ui.tenders.onetender.adapter.TenderOfferAdapter;
import com.tender.utils.ViewUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import gun0912.tedbottompicker.GridSpacingItemDecoration;
import kotlin.jvm.internal.Intrinsics;

import static android.content.Context.MODE_PRIVATE;
import static androidx.core.content.ContextCompat.checkSelfPermission;
import static com.tender.ui.tenders.onetender.adapter.TenderOfferAdapter.featureSelected;

public class OneTenderFragment extends BaseFragment implements OneTenderContract.View {

    private ArrayList<String> featureList = new ArrayList<>();
    private ArrayList<Feature> featureL = new ArrayList<>();

    private OneTenderPresenter presenter;
    private int PICK_FILE_REQUEST_CODE = 1;
    @BindView(R.id.features)
    TextView features;
    @BindView(R.id.featRc)
    RecyclerView featRc;
    @BindView(R.id.viewOffers)
    TextView viewOffers;
    @BindView(R.id.closeT)
    TextView closeT;
    @BindView(R.id.addOffer)
    TextView addOffer;
    public static ArrayList<Feature> meFeature = new ArrayList<>();
    private ArrayList<Feature> featureData = new ArrayList<>();
    public static int totalOffer = 0;
    private Tender tender;
    TenderFAdapter adapter;
    TenderOfferAdapter adapterOffer;
    Uri pdfFileUri = null;
    String pdfPath;

    public OneTenderFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_one_tender, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    private void setUpRecyclers(@NotNull RecyclerView recyclerView) {
        GridLayoutManager girdLayoutManager = new GridLayoutManager(this.getContext(), 1);
        recyclerView.setLayoutManager(girdLayoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration((new GridSpacingItemDecoration(1, ViewUtils.dpToPx(8.0F), true)));
    }


    public static OneTenderFragment newInstance(@NotNull String title, Tender tender) {
        Intrinsics.checkParameterIsNotNull(title, "title");
        Bundle bundle = new Bundle();
        bundle.putSerializable("tender", tender);
        OneTenderFragment fragment = new OneTenderFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    String type;


    @Override
    protected void setUp(View view) {
        Bundle bundle = getArguments();
        tender = (Tender) bundle.getSerializable("tender");
        if (tender != null) {
            adapter = new TenderFAdapter(response);
            adapterOffer = new TenderOfferAdapter(response);
            setUpRecyclers(featRc);
            Glide.with(this).load(tender.getImg()).into((ImageView) view.findViewById(R.id.tenderIV));
            TextView title = view.findViewById(R.id.titleTV);
            title.setText(tender.getTitle());
            TextView disc = view.findViewById(R.id.discTV);
            disc.setText(tender.getDisc());
            presenter = new OneTenderPresenter(new AppDataManager());
            presenter.onAttach(this);
            featureList = new ArrayList<>();
            presenter.getFeatures(tender.getId());
            SharedPreferences prefs = Objects.requireNonNull(getActivity()).getSharedPreferences("Tender", MODE_PRIVATE);
            type = prefs.getString("type", "type");
            if (type.equals("admin")) {
                viewOffers.setVisibility(View.VISIBLE);
                viewOffers.setOnClickListener(view1 -> {
                    getBaseActivity().replaceFragmentToActivity(ViewOffersFragment.newInstance("", tender), true);
                });
                closeT.setVisibility(View.VISIBLE);
                closeT.setOnClickListener(view1 -> {
                    tender.setType("closed");
                    presenter.closeTender(tender);
                });
            }
            if (tender.getType().equals("closed")) {
                viewOffers.setVisibility(View.GONE);
                addOffer.setVisibility(View.GONE);
                closeT.setVisibility(View.GONE);
            }
        }
    }
    TextView fileET;

    private void addOfferBottomSheet() {
        DialogSheet dialogSheet = new DialogSheet(Objects.requireNonNull(getContext()));
        dialogSheet.setTitle("Add offer")
                .setColoredNavigationBar(true)
                .setView(R.layout.offer_bottom_sheet)
                .setTitleTextSize(20)
                .setButtonsColorRes(R.color.colorAccent)
                .show();
        View inflatedView = dialogSheet.getInflatedView();
        Button offer = inflatedView.findViewById(R.id.addOffer);
        EditText priceET = inflatedView.findViewById(R.id.priceET);
        RecyclerView chipCloud = inflatedView.findViewById(R.id.features);
        fileET = inflatedView.findViewById(R.id.fileET);
        fileET.setOnClickListener(view -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED &&

                        checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.READ_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED &&

                        checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED) {

                    requestPermissions(new String[]{Manifest.permission.CAMERA,
                                    Manifest.permission.READ_EXTERNAL_STORAGE,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST);
                    return;
                }
            }
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.setType("application/pdf");
            startActivityForResult(i, PICK_FILE_REQUEST_CODE);
        });
        GridLayoutManager girdLayoutManager = new GridLayoutManager(this.getContext(), 1);
        chipCloud.setLayoutManager(girdLayoutManager);
        chipCloud.setAdapter(adapterOffer);
        chipCloud.addItemDecoration((new GridSpacingItemDecoration(1, ViewUtils.dpToPx(8.0F), true)));

        offer.setOnClickListener(view -> {
            if (priceET.getText() != null && !(priceET.getText().toString().isEmpty())) {
                if (!(Integer.parseInt(priceET.getText().toString()) <= 0)) {
                    if (pdfPath != null) {
                        try {
                            showLoading();
                            FirebaseStorage.getInstance().getReference(System.currentTimeMillis() + ".pdf").putFile(Uri.parse(pdfPath))
                                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                        @Override
                                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                            taskSnapshot.getMetadata()
                                                    .getReference()
                                                    .getDownloadUrl()
                                                    .addOnCompleteListener(new OnCompleteListener<Uri>() {
                                                           @Override
                                                           public void onComplete(@NonNull Task<Uri> task) {
                                                               Log.e("Ppppp", taskSnapshot.getMetadata().getReference().getDownloadUrl().toString());
                                                               Offer offer1 = new Offer(
                                                                       null,
                                                                       totalOffer,
                                                                       tender.getId(),
                                                                       FirebaseAuth.getInstance().getUid(),
                                                                       null,
                                                                       meFeature,
                                                                       Double.valueOf(priceET.getText().toString()),
                                                                       null,
                                                                       null,
                                                                       Objects.requireNonNull(task.getResult()).toString()
                                                               );
                                                               totalOffer = 0;
                                                               dialogSheet.dismiss();
                                                               presenter.addOffer(offer1, tender);
                                                           }
                                                       }
                                                    );
                                        }
                                    });
                        } catch (Exception e) {
                            Log.e("Ppppp", e.toString());
                        }

                    } else {
                        fileET.setError("File needed");
                    }

                } else {
                    priceET.setError("Enter a valid price");
                    priceET.requestFocus();
                }
            } else {
                priceET.setError("Enter a valid price");
                priceET.requestFocus();
            }
        });
    }

    private static final int MY_PERMISSIONS_REQUEST = 100;

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_FILE_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            assert data != null;
            pdfFileUri = data.getData();
            pdfPath = getPath(getContext(), pdfFileUri);
            pdfPath = "file:///" + pdfPath;
        }
    }

    private String getPath(final Context context, final Uri uri) {

        //check here to KITKAT or new version
        final boolean isKitKat = Build.VERSION.SDK_INT >=
                Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {

            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {
                if (pdfPath != null)
                    fileET.setText("");
                String pdfFileName = "File Selected";

                String id = DocumentsContract.getDocumentId(uri);
                if (!TextUtils.isEmpty(id)) {
                    if (id.startsWith("raw:")) {
                        return id.replaceFirst("raw:", "");
                    }
                    try {
                        final Uri contentUri = ContentUris.withAppendedId(
                                Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
//                        if (cursor != null) {
//                            cursor.moveToFirst();
//                            pdfFileName = cursor.getString(0);
//                            cursor.close();
//                        }
//                        else {
//                            String split = pdfFileUri.toString().split("/".toRegex()).dropLastWhile({ it.isEmpty() }).toTypedArray()
//                            if (!split.isEmpty() && split[split.length() - 1] != null) {
//                                pdfFileName = split[split.length() - 1].replace("%20", " ");
//                            }
//                        }
                        fileET.setText("Selected");
                        return getDataColumn(context, contentUri, null, null);
                    } catch (NumberFormatException e) {
                        Log.e("FileUtils", "Downloads provider returned unexpected uri " + uri.toString(), e);
                        return null;
                    }
                }
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    private String getDataColumn(Context context, Uri uri, String selection,
                                 String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                fileET.setText("Selected");
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return
                "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }


    public static boolean isGooglePhotosUri(Uri uri) {
        return
                "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    private void viewOffer(Offer offer) {

        DialogSheet dialogSheet = new DialogSheet(Objects.requireNonNull(getContext()));
        dialogSheet.setTitle("My offer")
                .setColoredNavigationBar(true)
                .setView(R.layout.offer_bottom_sheet)
                .setTitleTextSize(20)
                .setButtonsColorRes(R.color.colorAccent)
                .show();
        View inflatedView = dialogSheet.getInflatedView();
        Button offerB = inflatedView.findViewById(R.id.addOffer);
        fileET=inflatedView.findViewById(R.id.fileET);
        WebView webView = (WebView) inflatedView.findViewById(R.id.webview);
        ScrollView SWV=inflatedView.findViewById(R.id.SWV);
//        WebSettings webSettings = webView.getSettings();


//        webView.getSettings().setJavaScriptEnabled(true);
//
//        webView.loadUrl("https://docs.google.com/gview?embedded=true&url=" + offer.getFile());
//
//        webView.setWebViewClient(new WebViewClient() {
//            public void onPageFinished(WebView view, String url) {
//            }
//        });

        offerB.setText("update");
        fileET.setText("view offer file");
        fileET.setOnClickListener(view -> {
            //SWV.setVisibility(View.VISIBLE);
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(offer.getFile())));
        });
        EditText priceET = inflatedView.findViewById(R.id.priceET);
        priceET.setText(String.valueOf(offer.getPrice()));
        RecyclerView chipCloud = inflatedView.findViewById(R.id.features);
        GridLayoutManager girdLayoutManager = new GridLayoutManager(this.getContext(), 1);
        chipCloud.setLayoutManager(girdLayoutManager);
        chipCloud.setAdapter(adapterOffer);
        chipCloud.addItemDecoration((new GridSpacingItemDecoration(1, ViewUtils.dpToPx(8.0F), true)));
        featureSelected = offer.getFeature();

        offerB.setOnClickListener(view -> {
            if (priceET.getText() != null && !(priceET.getText().toString().isEmpty())) {
                if (!(Double.valueOf(priceET.getText().toString()) <= 0)) {
                    Offer offer1 = new Offer(
                            null,
                            totalOffer,
                            offer.gettId(),
                            FirebaseAuth.getInstance().getUid(),
                            null,
                            meFeature,
                            Double.valueOf(priceET.getText().toString()),
                            null, null, offer.getFile()
                    );

                    FirebaseDatabase.getInstance().getReference().child("offers").child(offer.getOfferId()).setValue(offer1)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
//                                    offer.setFeature(meFeature);
                                    meFeature.clear();
                                    meFeature = new ArrayList<>();
                                    totalOffer = 0;
                                    Toast.makeText(getContext(), "Offer updated", Toast.LENGTH_SHORT).show();
                                    dialogSheet.dismiss();
                                    getBaseActivity().getSupportFragmentManager().popBackStack();
                                }
                            });

                } else {
                    priceET.setError("Enter a valid price");
                    priceET.requestFocus();
                }
            } else {
                priceET.setError("Enter a valid price");
                priceET.requestFocus();
            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        meFeature.clear();
        meFeature = new ArrayList<>();
        response = new HashMap<String, ArrayList<Feature>>();
    }

    public static Map<String, ArrayList<Feature>> response = new HashMap<String, ArrayList<Feature>>();

    @Override
    public void feature(Feature feature) {
        Log.e("aaaa",feature.toString());
        if (!response.containsKey(feature.getFeature()))
            response.put(feature.getFeature(), new ArrayList<Feature>());
        response.get(feature.getFeature()).add(feature);
        adapter.addFeaturers(response);
        featureList.add(feature.getTitle());
        featureL.add(feature);
        featureData.add(feature);


    }

    @Override
    public void onResume() {
        super.onResume();
        response = new HashMap<String, ArrayList<Feature>>();
        featureList = new ArrayList<>();
    }

    @Override
    public void empty() {
        features.setText("No features");
    }

    @Override
    public void updated(Tender tender) {
        Toast.makeText(getContext(), "Tender closed", Toast.LENGTH_SHORT).show();
        getBaseActivity().getSupportFragmentManager().popBackStack();
    }

    @Override
    public void offerAdded(Offer offer) {
        meFeature.clear();
        meFeature = new ArrayList<>();
        Toast.makeText(getContext(), "Offer added", Toast.LENGTH_SHORT).show();
        getBaseActivity().getSupportFragmentManager().popBackStack();
    }

    @Override
    public void offered(Offer offer) {
        if (type.equals("admin")) {
            viewOffers.setVisibility(View.VISIBLE);
            viewOffers.setOnClickListener(view1 -> {
                getBaseActivity().replaceFragmentToActivity(ViewOffersFragment.newInstance("", tender), true);
            });
            closeT.setVisibility(View.VISIBLE);
            closeT.setOnClickListener(view1 -> {
                tender.setType("closed");
                presenter.closeTender(tender);
            });
        } else {
            meFeature = new ArrayList<>();
            viewOffers.setVisibility(View.GONE);
            closeT.setVisibility(View.GONE);
            addOffer.setVisibility(View.VISIBLE);
            addOffer.setText("My offer");
            addOffer.setOnClickListener(view -> {
                viewOffer(offer);
            });
        }

    }

    @Override
    public void noOffer() {
        if (type.equals("admin")) {
            viewOffers.setVisibility(View.VISIBLE);
            viewOffers.setOnClickListener(view1 -> {
                getBaseActivity().replaceFragmentToActivity(ViewOffersFragment.newInstance("", tender), true);
            });
            closeT.setVisibility(View.VISIBLE);
            closeT.setOnClickListener(view1 -> {
                tender.setType("closed");
                presenter.closeTender(tender);
            });
        } else {
            viewOffers.setVisibility(View.GONE);
            closeT.setVisibility(View.GONE);
            addOffer.setVisibility(View.VISIBLE);
            addOffer.setOnClickListener(view1 -> {
//                getBaseActivity().replaceFragmentToActivity(AddOfferFragment.newInstance("",tender),true);
                addOfferBottomSheet();
            });
        }
    }

}
