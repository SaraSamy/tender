package com.tender.ui.tenders.add;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.tender.R;
import com.tender.model.Tender;

import butterknife.BindView;
import butterknife.ButterKnife;


class FeaturesVH extends RecyclerView.ViewHolder {

    @BindView(R.id.nameTV)
    TextView nameTV;

    FeaturesVH(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind(Tender tender) {
        nameTV.setText(tender.getTitle());
    }
}
