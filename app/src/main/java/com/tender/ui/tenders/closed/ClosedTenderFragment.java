package com.tender.ui.tenders.closed;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.tender.R;
import com.tender.data.AppDataManager;
import com.tender.model.Tender;
import com.tender.ui.base.BaseFragment;
import com.tender.ui.tenders.TenderContract;
import com.tender.ui.tenders.TenderPresenter;
import com.tender.ui.tenders.adapter.TenderAdapter;
import com.tender.ui.tenders.add.AddTenderFragment;
import com.tender.ui.tenders.onetender.OneTenderFragment;
import com.tender.utils.ViewUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import gun0912.tedbottompicker.GridSpacingItemDecoration;
import kotlin.jvm.internal.Intrinsics;


public class ClosedTenderFragment extends BaseFragment implements TenderContract.View, TenderAdapter.TenderData {


    @BindView(R.id.tendersRV)
    RecyclerView tendersRV;
    TenderPresenter presenter;
    TenderAdapter adapter;
    private ArrayList<Tender> tenders=new ArrayList<>();

    public ClosedTenderFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_closed_tender, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    protected void setUp(View view) {
        adapter = new TenderAdapter(tenders,this);
        presenter =new TenderPresenter(new AppDataManager());
        presenter.onAttach(this);
        tenders =new ArrayList<>();
        presenter.getTender("closed");
        setUpRecyclers(tendersRV);
    }

    private void setUpRecyclers(@NotNull RecyclerView recyclerView) {
        GridLayoutManager girdLayoutManager = new GridLayoutManager(this.getContext(), 1);
        recyclerView.setLayoutManager(girdLayoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration((new GridSpacingItemDecoration(1, ViewUtils.dpToPx(8.0F), true)));
    }

    @Override
    public void tender(Tender tender) {
        View view = getView();
        if(view!=null){
            view.findViewById(R.id.NoDataTV).setVisibility(View.GONE);
            view.findViewById(R.id.NoDataIV).setVisibility(View.GONE);
            view.findViewById(R.id.tendersRV).setVisibility(View.VISIBLE);
        }
        adapter.addTenderr(tender);
    }

    @Override
    public void empty() {
        View view = getView();
        if(view!=null){
            view.findViewById(R.id.NoDataTV).setVisibility(View.VISIBLE);
            view.findViewById(R.id.NoDataIV).setVisibility(View.VISIBLE);
            view.findViewById(R.id.tendersRV).setVisibility(View.GONE);
        }
    }

    @Override
    public void tenderView(Tender tender) {
        getBaseActivity().replaceFragmentToActivity(OneTenderFragment.newInstance("",tender), true);

    }

    public static ClosedTenderFragment newInstance(@NotNull String title) {
        Intrinsics.checkParameterIsNotNull(title, "title");
        return new ClosedTenderFragment();
    }

}
