package com.tender.ui.home;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.tender.R;
import com.tender.ui.base.BaseFragment;
import com.tender.ui.companies.AllCompaniesFragment;
import com.tender.ui.login.LoginActivity;
import com.tender.ui.notification.NotificationFragment;
import com.tender.ui.profile.ProfileFragment;
import com.tender.ui.search.SearchFragment;
import com.tender.ui.tenders.TendersFragment;
import com.tender.ui.tenders.closed.ClosedTenderFragment;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;

public class HomeFragment extends BaseFragment {


    @BindView(R.id.background)
    ImageView background;
    @BindView(R.id.profileCV)
    CardView profileCV;
    @BindView(R.id.tendersCV)
    CardView tendersCV;
    @BindView(R.id.companyIV)
    ImageView companyIV;
    @BindView(R.id.searchCV)
    CardView searchCV;
    @BindView(R.id.notificationCV)
    CardView notificationCV;
    @BindView(R.id.closedTendersCV)
    CardView closedTendersCV;
    @BindView(R.id.allCompaniesCV)
    CardView allCompaniesCV;
    @BindView(R.id.adminV)
    LinearLayout adminV;

    public HomeFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Glide.with(this).load(R.drawable.background).into(background);
    }

    @Override
    protected void setUp(View view) {
        SharedPreferences prefs = Objects.requireNonNull(getActivity()).getSharedPreferences("Tender", MODE_PRIVATE);
        String type = prefs.getString("type", "type");
        if (type.equals("admin")) {
            adminV.setVisibility(View.VISIBLE);
        }
        profileCV.setOnClickListener(view1 -> {
            getBaseActivity().replaceFragmentToActivity(ProfileFragment.newInstance(""), true);
        });
        tendersCV.setOnClickListener(view1 -> {
            getBaseActivity().replaceFragmentToActivity(TendersFragment.newInstance(""), true);
        });
        closedTendersCV.setOnClickListener(view1 -> {
            getBaseActivity().replaceFragmentToActivity(ClosedTenderFragment.newInstance(""), true);
        });
        allCompaniesCV.setOnClickListener(view1 -> {
            getBaseActivity().replaceFragmentToActivity(AllCompaniesFragment.newInstance(""), true);
        });
        searchCV.setOnClickListener(view1 -> {
            FirebaseAuth.getInstance().signOut();
            Intent logout = new Intent(getContext(), LoginActivity.class);
            logout.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(logout);
            getActivity().finish();

        });
        notificationCV.setOnClickListener(view1 -> {
            getBaseActivity().replaceFragmentToActivity(NotificationFragment.newInstance(""), true);
        });

    }
}
