package com.tender.ui.profile.setting;


import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.tender.R;
import com.tender.data.AppDataManager;
import com.tender.model.User;
import com.tender.ui.base.BaseFragment;
import com.tender.ui.profile.ProfileContract;
import com.tender.ui.profile.ProfilePresenter;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gun0912.tedbottompicker.TedRxBottomPicker;
import kotlin.jvm.internal.Intrinsics;

import static androidx.core.content.ContextCompat.checkSelfPermission;

public class SettingFragment extends BaseFragment implements ProfileContract.View {

    @BindView(R.id.userIV)
    ImageView userIV;
    @BindView(R.id.imageChooser)
    ImageView imageChooser;
    @BindView(R.id.nameET)
    EditText nameET;
    @BindView(R.id.bioET)
    EditText bioET;
    @BindView(R.id.addressET)
    EditText addressET;
    @BindView(R.id.updateB)
    Button updateB;
    ProfilePresenter presenter;
    private static final int MY_PERMISSIONS_REQUEST = 100;
    String image = "";
    Boolean flag=false;
    User me;

    public SettingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void setUp(View view) {
        presenter = new ProfilePresenter(new AppDataManager());
        presenter.onAttach(this);
        imageChooser.setOnClickListener(view1 -> {
            getImage();
        });
        Bundle bundle  =getArguments();
        assert bundle != null;
        User user= (User)bundle.getSerializable("user");
        if(user!=null){
            me=user;
            if (user.getImg() != null) {
                Glide.with(this).load(user.getImg()).into(userIV);
            }
            nameET.setText(user.getName());

            if (user.getBio() != null && !(user.getBio().equals(""))) {
                bioET.setText(user.getBio());
            }
            if (user.getAddress() != null && !(user.getAddress().equals(""))) {
                addressET.setText(user.getAddress());
            }
        }
    }

    @Override
    public void onSuccess(User user) {
        Toast.makeText(getContext(),"Profile updated",Toast.LENGTH_SHORT).show();
        getBaseActivity().getSupportFragmentManager().popBackStack();
    }

    @OnClick(R.id.updateB)
    void onViewClicked() {
        if (bioET.getText() != null && !(bioET.getText().toString().equals(""))) {
            me.setBio(bioET.getText().toString());
        }
        if (addressET.getText() != null && !(addressET.getText().toString().equals(""))) {
            me.setAddress(addressET.getText().toString());
        }
        if (nameET.getText() != null && !(nameET.getText().toString().equals(""))) {
            me.setName(nameET.getText().toString());
        }
        if(!image.equals(""))
            me.setImg(image);
        presenter.update(me,flag);
    }

    private void getImage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED &&

                    checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED &&

                    checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {

                requestPermissions(new String[]{Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST);
                return;
            }
        }
        TedRxBottomPicker.with(getActivity())
                .show()
                .subscribe(uri -> {
                            Glide.with(this)
                                    .load(uri)
                                    .into(userIV);
                            image = uri.toString();
                            flag=true;
                        },
                        throwable -> {
                        });
    }

    public static SettingFragment newInstance(@NotNull String title, User user) {
        Intrinsics.checkParameterIsNotNull(title, "title");
        Bundle bundle = new Bundle();
        bundle.putSerializable("user", user);
        SettingFragment fragment = new SettingFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

}
