package com.tender.ui.profile;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.marcoscg.dialogsheet.DialogSheet;
import com.tender.R;
import com.tender.data.AppDataManager;
import com.tender.model.User;
import com.tender.ui.base.BaseFragment;
import com.tender.ui.profile.setting.SettingFragment;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import kotlin.jvm.internal.Intrinsics;

public class ProfileFragment extends BaseFragment implements ProfileContract.View {

    ProfilePresenter presenter;
    @BindView(R.id.setting)
    ImageView setting;
    @BindView(R.id.userTIV)
    ImageView userTIV;

    public ProfileFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        Glide.with(this).load(R.drawable.background).into(userTIV);
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    protected void setUp(View view) {
        presenter = new ProfilePresenter(new AppDataManager());
        presenter.onAttach(this);
        presenter.getUser();
    }

    public static ProfileFragment newInstance(@NotNull String title) {
        Intrinsics.checkParameterIsNotNull(title, "title");
        return new ProfileFragment();
    }


    @Override
    public void onSuccess(User user) {
        View view = getView();
        if (view != null) {
            setting.setOnClickListener(view1 -> {
                getBaseActivity().replaceFragmentToActivity(SettingFragment.newInstance("", user), true);
            });
            if (user.getImg() != null) {
                Glide.with(this).load(user.getImg()).into((ImageView) view.findViewById(R.id.userIV));
            }
            TextView name = view.findViewById(R.id.nameTV);
            name.setText(user.getName());

            if (user.getBio() != null) {
                TextView bio = view.findViewById(R.id.bio);
                bio.setText(user.getBio());
            }

            if (user.getEmail() != null) {
                TextView email = view.findViewById(R.id.email);
                email.setText(user.getEmail());
            }
            if (user.getAddress() != null) {
                TextView location = view.findViewById(R.id.location);
                location.setText(user.getAddress());
            }
            view.findViewById(R.id.logoutB).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    DialogSheet dialogSheet = new DialogSheet(Objects.requireNonNull(getContext()));
                    dialogSheet.setTitle("Change password")
                            .setColoredNavigationBar(true)
                            .setView(R.layout.change_password)
                            .setTitleTextSize(20)
                            .setButtonsColorRes(R.color.colorAccent)
                            .show();
                    View inflatedView = dialogSheet.getInflatedView();
                    Button changeB = inflatedView.findViewById(R.id.changeB);
                    EditText old = inflatedView.findViewById(R.id.oldET);
                    EditText newET = inflatedView.findViewById(R.id.newET);
                    changeB.setOnClickListener(view1 -> {
                        if (old.getText().toString().isEmpty()) {
                            old.setError("required");
                            old.requestFocus();
                            return;
                        }
                        if (newET.getText().toString().isEmpty()) {
                            newET.setError("required");
                            newET.requestFocus();
                            return;
                        } else {
                            showLoading();
                            AuthCredential credential = EmailAuthProvider
                                    .getCredential(FirebaseAuth.getInstance().getCurrentUser().getEmail(), old.getText().toString());
                            FirebaseAuth.getInstance().getCurrentUser().reauthenticate(credential)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                FirebaseAuth.getInstance().getCurrentUser()
                                                        .updatePassword(newET.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> taskA) {
                                                        if (task.isSuccessful()) {
                                                            hideLoading();
                                                            Toast.makeText(getContext(), "Password updated", Toast.LENGTH_SHORT).show();
                                                            dialogSheet.dismiss();
                                                        } else {
                                                            hideLoading();
                                                            dialogSheet.dismiss();
                                                            Toast.makeText(getContext(), "Error password not updated", Toast.LENGTH_SHORT).show();
                                                            Log.e("fff", taskA.getException().getMessage());
                                                        }
                                                    }
                                                });
                                            } else {
                                                Log.e("fffs", task.getException().getMessage());
                                                Toast.makeText(getContext(), "Error password invalid", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                        }
                    });
                }
            });
        }
    }
}
