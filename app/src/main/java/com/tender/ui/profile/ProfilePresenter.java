package com.tender.ui.profile;

import android.net.Uri;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.UploadTask;
import com.tender.R;
import com.tender.data.DataManager;
import com.tender.model.User;
import com.tender.ui.base.BaseLisener;
import com.tender.ui.base.BasePresenter;
import com.tender.ui.tenders.add.AddTenderPresenter;

import java.util.Objects;

public class ProfilePresenter <V extends ProfileContract.View> extends BasePresenter<V>
        implements ProfileContract.Presenter<V> , BaseLisener<User, String,String> {

    public ProfilePresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void onSuccess(User user) {
        if (isViewAttached()) {
            getMvpView().hideLoading();
            getMvpView().onSuccess(user);
        }
    }

    @Override
    public void onSuccessData(User data, String key) {

    }

    @Override
    public void onFail(String error) {
        if (isViewAttached()) {
            getMvpView().hideLoading();
            getMvpView().showMessage(error);
        }
    }

    @Override
    public void getUser() {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            getDataManager().getUser(FirebaseAuth.getInstance().getUid(),this);
        } else {
            getMvpView().showMessage(R.string.error_no_iternet_connection);
        }
    }

    @Override
    public void update(User user,Boolean flag) {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            if(flag) {
                getDataManager().uploadImage(Uri.parse(user.getImg())).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        taskSnapshot.getMetadata()
                                .getReference()
                                .getDownloadUrl()
                                .addOnCompleteListener(new OnCompleteListener<Uri>() {
                                                           @Override
                                                           public void onComplete(@NonNull Task<Uri> task) {
                                                               user.setImg(Objects.requireNonNull(task.getResult()).toString());
                                                               getDataManager().updateUser(user, ProfilePresenter.this);
                                                           }
                                                       }
                                );
                    }
                });
            }
            else{
                getDataManager().updateUser(user, ProfilePresenter.this);
            }
        } else {
            getMvpView().showMessage(R.string.error_no_iternet_connection);
        }
    }
}
