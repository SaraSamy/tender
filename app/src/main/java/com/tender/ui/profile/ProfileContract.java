package com.tender.ui.profile;

import com.tender.model.User;
import com.tender.ui.base.MvpPresenter;
import com.tender.ui.base.MvpView;

public interface ProfileContract {
     interface View extends MvpView {
        void onSuccess(User user);
    }

    interface Presenter<V extends View> extends MvpPresenter<V> {
        void getUser();
        void update(User user,Boolean flag);
    }
}
