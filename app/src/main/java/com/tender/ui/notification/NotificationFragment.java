package com.tender.ui.notification;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tender.R;
import com.tender.data.AppDataManager;
import com.tender.model.Notification;
import com.tender.ui.base.BaseFragment;
import com.tender.ui.notification.adapter.NotificationAdapter;
import com.tender.ui.tenders.onetender.OneTenderFragment;
import com.tender.utils.ViewUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import gun0912.tedbottompicker.GridSpacingItemDecoration;
import kotlin.jvm.internal.Intrinsics;

import static android.content.Context.MODE_PRIVATE;

public class NotificationFragment extends BaseFragment implements NotificationAdapter.NotificationI, NotificationContract.View {

    @BindView(R.id.notificationRV)
    RecyclerView notificationRV;
    @BindView(R.id.NoDataIV)
    ImageView NoDataIV;
    @BindView(R.id.NoDataTV)
    TextView NoDataTV;

    public NotificationFragment() {
    }

    NotificationAdapter adapter;
    private ArrayList<Notification> notifications = new ArrayList<>();
    NotificationPresenter presenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    private void setUpRecyclers(@NotNull RecyclerView recyclerView) {
        GridLayoutManager girdLayoutManager = new GridLayoutManager(this.getContext(), 1);
        recyclerView.setLayoutManager(girdLayoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration((new GridSpacingItemDecoration(1, ViewUtils.dpToPx(8.0F), true)));
    }

    @Override
    protected void setUp(View view) {
        notifications = new ArrayList<>();
        adapter = new NotificationAdapter(notifications, this);
        presenter = new NotificationPresenter(new AppDataManager());
        presenter.onAttach(this);
        SharedPreferences prefs = Objects.requireNonNull(getActivity()).getSharedPreferences("Tender", MODE_PRIVATE);
        String type = prefs.getString("type", "type");
        if (type.equals("admin")) {
            presenter.getNotification("offerAdded");
        }
        else
            presenter.getNotification("tenderAdded");
        setUpRecyclers(notificationRV);
    }

    @Override
    public void notificationView(Notification notification) {
//        getBaseActivity().replaceFragmentToActivity(OneTenderFragment.newInstance("",tender), true);
    }

    @Override
    public void notificationCome(Notification notification) {
        View view = getView();
        if(view!=null){
            view.findViewById(R.id.NoDataTV).setVisibility(View.GONE);
            view.findViewById(R.id.NoDataIV).setVisibility(View.GONE);
            view.findViewById(R.id.notificationRV).setVisibility(View.VISIBLE);
        }
        adapter.addNotificationVH(notification);
    }

    @Override
    public void empty() {
        View view = getView();
        if(view!=null){
            view.findViewById(R.id.NoDataTV).setVisibility(View.VISIBLE);
            view.findViewById(R.id.NoDataIV).setVisibility(View.VISIBLE);
            view.findViewById(R.id.notificationRV).setVisibility(View.GONE);
        }
    }

    public static NotificationFragment newInstance(@NotNull String title) {
        Intrinsics.checkParameterIsNotNull(title, "title");
        return new NotificationFragment();
    }
}
