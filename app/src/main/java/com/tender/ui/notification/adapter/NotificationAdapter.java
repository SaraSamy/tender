package com.tender.ui.notification.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.tender.R;
import com.tender.model.Notification;
import java.util.ArrayList;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationVH> {

    private ArrayList<Notification> notifications;
    private NotificationI notification;

    public NotificationAdapter(ArrayList<Notification> notifications, NotificationI notification) {
        this.notifications = notifications;
        this.notification=notification;
    }

    @Override
    public NotificationVH onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NotificationVH(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_notification, null));
    }

    @Override
    public void onBindViewHolder(NotificationVH holder, int position) {
        holder.bind(notifications.get(position));
        holder.itemView.setOnClickListener(view -> {
            notification.notificationView(notifications.get(position));
        });
    }

    @Override
    public int getItemCount() {
        return notifications.size();
    }

    public void addNotificationVH(Notification notification) {
        if (notification != null) {
            notifications.add(notification);
            notifyDataSetChanged();
        }
    }


    public void addNotificationVHs(ArrayList< Notification> notifications1) {
        if (notifications1 != null) {
            this.notifications.addAll(notifications1);
            notifyDataSetChanged();
        }
    }

    public ArrayList< Notification> getNotification() {
        return notifications;
    }

    public interface NotificationI {
        void notificationView(Notification notification);
    }
}
