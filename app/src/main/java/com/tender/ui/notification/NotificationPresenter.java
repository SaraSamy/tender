package com.tender.ui.notification;

import android.util.Log;

import com.tender.R;
import com.tender.data.DataManager;
import com.tender.model.Notification;
import com.tender.model.User;
import com.tender.ui.base.BaseLisener;
import com.tender.ui.base.BasePresenter;
import com.tender.ui.companies.AllCompaniesContract;

public class NotificationPresenter<V extends NotificationContract.View> extends BasePresenter<V>
        implements NotificationContract.Presenter<V> , BaseLisener<Notification, String,String> {

    public NotificationPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void onSuccess(Notification data) {

    }

    @Override
    public void onSuccessData(Notification data, String key) {
        if (isViewAttached()) {
//            data.setId(key);
            getMvpView().hideLoading();
            getMvpView().notificationCome(data);

        }
    }

    @Override
    public void onFail(String error) {
        if (isViewAttached()) {
            getMvpView().hideLoading();
            if (error.equals("no data")) {
                getMvpView().empty();
            } else
                getMvpView().showMessage(error);
        }
    }

    @Override
    public void getNotification(String eq) {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            getDataManager().getData("notification","type",eq,this,Notification.class);
        } else {
            getMvpView().showMessage(R.string.error_no_iternet_connection);
        }
    }
}
