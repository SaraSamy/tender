package com.tender.ui.notification;

import com.tender.model.Notification;
import com.tender.model.User;
import com.tender.ui.base.MvpPresenter;
import com.tender.ui.base.MvpView;

public interface NotificationContract {
     interface View extends MvpView {
        void notificationCome(Notification notification);
         void empty();
    }

    interface Presenter<V extends View> extends MvpPresenter<V> {
         void getNotification(String eq);
    }
}
