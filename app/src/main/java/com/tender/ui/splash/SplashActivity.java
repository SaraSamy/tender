package com.tender.ui.splash;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.tender.R;
import com.tender.ui.login.LoginActivity;
import com.tender.ui.main.MainActivity;

public class SplashActivity extends AppCompatActivity  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        (new Handler()).postDelayed((Runnable) (new Runnable() {
            public final void run() {

                FirebaseAuth mAuth = FirebaseAuth.getInstance();
                Log.e("act.view", String.valueOf((mAuth.getCurrentUser()== null)));

                SplashActivity.this.startActivity(( mAuth.getCurrentUser()) == null ?
                        LoginActivity.getStartIntent( SplashActivity.this):
                        MainActivity.getStartIntent(SplashActivity.this));
                SplashActivity.this.finish();
            }
        }), 4000L);
    }
}
