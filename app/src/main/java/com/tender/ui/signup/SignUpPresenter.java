package com.tender.ui.signup;

import com.tender.R;
import com.tender.data.DataManager;
import com.tender.model.User;
import com.tender.ui.base.BaseLisener;
import com.tender.ui.base.BasePresenter;
import com.tender.utils.CommonUtils;

public class SignUpPresenter <V extends SignUpContract.View>
        extends BasePresenter<V> implements SignUpContract.Presenter<V>, BaseLisener<String, String,String> {

    public SignUpPresenter(DataManager dataManager) {
        super(dataManager);
    }


    @Override
    public void signUp(String email, String password, User user) {

        if (email.isEmpty()) {
            getMvpView().showMessage(R.string.error_email_required);
            return;
        } else if (!CommonUtils.isEmailValid(email)) {
            getMvpView().showMessage(R.string.error_invalid_email);
            return;
        }
        if (password.isEmpty()) {
            getMvpView().showMessage(R.string.error_password_empty);
            return;
        }

        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            getDataManager().signUp(email, password, this,user);
        } else {
            getMvpView().showMessage(R.string.error_no_iternet_connection);
        }
    }

    @Override
    public void onSuccess(String userId) {
        getMvpView().hideLoading();
        if (isViewAttached())
            getMvpView().onSignUpSuccess();
    }

    @Override
    public void onSuccessData(String data, String key) {

    }

    @Override
    public void onFail(String error) {
        if (isViewAttached()) {
            getMvpView().hideLoading();
            getMvpView().showMessage(error);
        }
    }
}
