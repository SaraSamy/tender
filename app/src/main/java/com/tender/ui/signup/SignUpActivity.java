package com.tender.ui.signup;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.tender.R;
import com.tender.data.AppDataManager;
import com.tender.model.User;
import com.tender.ui.base.BaseActivity;
import com.tender.ui.main.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SignUpActivity extends BaseActivity implements SignUpContract.View {

    SignUpContract.Presenter<SignUpContract.View> presenter;
    @BindView(R.id.background)
    ImageView background;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.confirmPassword)
    EditText confirmPassword;
    @BindView(R.id.SignupB)
    Button SignupB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        Glide.with(this).load(R.drawable.background).into(background);
        setUp();
    }

    @Override
    protected void setUp() {
        presenter = new SignUpPresenter(new AppDataManager());
        presenter.onAttach(this);
    }

    @Override
    public void onSignUpSuccess() {
        SharedPreferences.Editor editor = getSharedPreferences("Tender", MODE_PRIVATE).edit();
        editor.putString("type", "company");
        editor.apply();
        Intent signUp = new Intent(SignUpActivity.this, MainActivity.class);
        signUp.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(signUp);
        finish();
    }

    public void signup(View view) {

        EditText email=findViewById(R.id.email);
        EditText password=findViewById(R.id.password);
        EditText confirmPassword=findViewById(R.id.confirmPassword);
        EditText name = findViewById(R.id.name);

        if (email.getText().toString().trim().isEmpty()) {
            email.setError("Email is required");
            email.requestFocus();
            return;
        }
        if (name.getText().toString().isEmpty()) {
            name.setError("name is required");
            name.requestFocus();
            return;
        }
        if (password.getText().toString().isEmpty()) {
            password.setError("Password is required");
            password.requestFocus();
            return;
        }
        if (password.length() < 6) {
            confirmPassword.setError("Password can not be less than 6 characters");
            confirmPassword.requestFocus();
            return;
        }
        if (confirmPassword.getText().toString().isEmpty()||!(confirmPassword.getText().toString().equals(password.getText().toString()))) {
            confirmPassword.setError("confirm password must match your password");
            confirmPassword.requestFocus();
            return;
        }
        else{
            presenter.signUp(email.getText().toString(),password.getText().toString(),new User(
                    name.getText().toString(),
                    "https://firebasestorage.googleapis.com/v0/b/tenderapp-bfd7e.appspot.com/o/work.png?alt=media&token=6ab7d9a1-db19-4406-8ea4-ae477fc72f23",
                    "company",
                    "",
                    "",
                    email.getText().toString(),
                    ""
            ));
        }
    }

    public static Intent getStartIntent(Context context) {
        return new Intent(context, SignUpActivity.class);
    }
}
