package com.tender.ui.signup;

import com.tender.model.User;
import com.tender.ui.base.MvpPresenter;
import com.tender.ui.base.MvpView;

public interface SignUpContract {

    interface View extends MvpView {
        void onSignUpSuccess();
    }

    interface Presenter<V extends View> extends MvpPresenter<V> {
        void signUp(String email, String password, User user);
    }
}
