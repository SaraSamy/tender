package com.tender.ui.login;

import com.tender.model.User;
import com.tender.ui.base.MvpPresenter;
import com.tender.ui.base.MvpView;

public interface LoginContract {

    public interface View extends MvpView {
        void onLoginSuccess(User data);
    }

    interface Presenter<V extends View> extends MvpPresenter<V> {
    void login(String email, String password);
    }
}