package com.tender.ui.login;

import com.tender.R;
import com.tender.data.DataManager;
import com.tender.model.User;
import com.tender.ui.base.BaseLisener;
import com.tender.ui.base.BasePresenter;

public class LoginPresenter <V extends LoginContract.View> extends BasePresenter<V>
        implements LoginContract.Presenter<V> , BaseLisener<User, String,String> {

    public LoginPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void login(String email, String password) {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            getDataManager().login(email, password, this);
        } else {
            getMvpView().showMessage(R.string.error_no_iternet_connection);
        }
    }

    @Override
    public void onSuccess(User data) {
        if (isViewAttached()) {
            getMvpView().hideLoading();
            getMvpView().onLoginSuccess(data);
        }
    }

    @Override
    public void onSuccessData(User data, String key) {

    }

    @Override
    public void onFail(String error) {
        if (isViewAttached()) {
            getMvpView().hideLoading();
            getMvpView().showMessage(error);
        }
    }
}
