package com.tender.ui.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.tender.R;
import com.tender.data.AppDataManager;
import com.tender.model.User;
import com.tender.ui.base.BaseActivity;
import com.tender.ui.main.MainActivity;
import com.tender.ui.signup.SignUpActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends BaseActivity implements LoginContract.View {

    EditText email;
    EditText password;
    LoginPresenter presenter;
    @BindView(R.id.background)
    ImageView background;

    @Override
    protected void setUp() {
        setUnBinder(ButterKnife.bind(this));
    }

    public void goToSignUp(View view) {
        startActivity(SignUpActivity.getStartIntent(this));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        Glide.with(this).load(R.drawable.background).into(background);
        presenter = new LoginPresenter(new AppDataManager());
        presenter.onAttach(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    public static Intent getStartIntent(Context context) {
        return new Intent(context, LoginActivity.class);
    }

    public void onLogin(View view) {
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        if (email.getText().toString().isEmpty()) {
            email.setError("Email is required");
            email.requestFocus();
            return;
        }
        if (password.getText().toString().isEmpty()) {
            password.setError("Password is required");
            password.requestFocus();
            return;
        } else
            presenter.login(email.getText().toString().trim(), password.getText().toString());
    }

    @Override
    public void onLoginSuccess(User data) {
        SharedPreferences.Editor editor = getSharedPreferences("Tender", MODE_PRIVATE).edit();
        editor.putString("type", data.getType());
        editor.apply();
        Intent login = new Intent(LoginActivity.this, MainActivity.class);
        login.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(login);
        finish();
    }
}
