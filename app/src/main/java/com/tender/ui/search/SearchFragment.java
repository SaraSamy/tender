package com.tender.ui.search;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tender.R;
import com.tender.data.AppDataManager;
import com.tender.model.Tender;
import com.tender.ui.base.BaseFragment;
import com.tender.ui.tenders.adapter.TenderAdapter;
import com.tender.ui.tenders.onetender.OneTenderFragment;
import com.tender.utils.ViewUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import gun0912.tedbottompicker.GridSpacingItemDecoration;
import kotlin.jvm.internal.Intrinsics;

public class SearchFragment extends BaseFragment implements  SearchContract.View, TenderAdapter.TenderData {


    @BindView(R.id.searchView)
    SearchView searchView;
    @BindView(R.id.searchRV)
    RecyclerView searchRV;
    @BindView(R.id.searchIV)
    ImageView searchIV;
    TenderAdapter adapter;
    SearchPresenter presenter;
    @BindView(R.id.NoDataIV)
    ImageView NoDataIV;
    @BindView(R.id.NoDataTV)
    TextView NoDataTV;
    private ArrayList<Tender> tenders = new ArrayList<>();


    public SearchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    private void setUpRecyclers(@NotNull RecyclerView recyclerView) {
        GridLayoutManager girdLayoutManager = new GridLayoutManager(this.getContext(), 1);
        recyclerView.setLayoutManager(girdLayoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration((new GridSpacingItemDecoration(1, ViewUtils.dpToPx(8.0F), true)));
    }

    @Override
    protected void setUp(View view) {
        presenter = new SearchPresenter(new AppDataManager());
        presenter.onAttach(this);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                searchIV.setVisibility(View.GONE);
                tenders = new ArrayList<>();
                adapter = new TenderAdapter(tenders, SearchFragment.this::tenderView);
                presenter.search(s);
                setUpRecyclers(searchRV);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
    }

    public static SearchFragment newInstance(@NotNull String title) {
        Intrinsics.checkParameterIsNotNull(title, "title");
        return new SearchFragment();
    }

    @Override
    public void onSuccess(Tender tender) {
        View view = getView();
        if (view != null) {
            view.findViewById(R.id.NoDataTV).setVisibility(View.GONE);
            view.findViewById(R.id.NoDataIV).setVisibility(View.GONE);
            view.findViewById(R.id.searchRV).setVisibility(View.VISIBLE);
        }
        adapter.addTenderr(tender);
    }

    @Override
    public void tenderView(Tender tender) {
        getBaseActivity().replaceFragmentToActivity(OneTenderFragment.newInstance("",tender), true);
    }

    public void empty() {
        View view = getView();
        if (view != null) {
            view.findViewById(R.id.NoDataTV).setVisibility(View.VISIBLE);
            view.findViewById(R.id.NoDataIV).setVisibility(View.VISIBLE);
            view.findViewById(R.id.searchRV).setVisibility(View.GONE);
        }
    }
}
