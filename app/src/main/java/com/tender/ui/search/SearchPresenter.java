package com.tender.ui.search;

import com.google.firebase.auth.FirebaseAuth;
import com.tender.R;
import com.tender.data.DataManager;
import com.tender.model.Tender;
import com.tender.model.User;
import com.tender.ui.base.BaseLisener;
import com.tender.ui.base.BasePresenter;
import com.tender.ui.profile.ProfileContract;

public class SearchPresenter<V extends SearchContract.View> extends BasePresenter<V>
        implements SearchContract.Presenter<V> , BaseLisener<Tender, String,String> {

    public SearchPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void onSuccess(Tender tender) {
        if (isViewAttached()) {
            getMvpView().hideLoading();
            getMvpView().onSuccess(tender);
        }
    }

    @Override
    public void onSuccessData(Tender data, String key) {
        if (isViewAttached()) {
            data.setId(key);
            getMvpView().hideLoading();
            getMvpView().onSuccess(data);

        }
    }

    @Override
    public void onFail(String error) {
        if (isViewAttached()) {
            getMvpView().hideLoading();
            if (error.equals("no data")) {
                getMvpView().empty();
            } else
                getMvpView().showMessage(error);
        }
    }

    @Override
    public void search(String title) {
        if (getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            getDataManager().search("tenders","title",title,this,Tender.class);
        } else {
            getMvpView().showMessage(R.string.error_no_iternet_connection);
        }
    }
}
