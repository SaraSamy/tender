package com.tender.ui.search;

import com.tender.model.Tender;
import com.tender.model.User;
import com.tender.ui.base.MvpPresenter;
import com.tender.ui.base.MvpView;

public interface SearchContract {
     interface View extends MvpView {
         void empty();
        void onSuccess(Tender tender);
    }

    interface Presenter<V extends View> extends MvpPresenter<V> {
        void search(String title);
    }
}
