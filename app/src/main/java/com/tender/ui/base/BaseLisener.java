package com.tender.ui.base;

/**
 * Created by Mohamed Fakhry on 23/02/2018.
 */

public interface BaseLisener<T, E,K> {

    void onSuccess(T data);

    void onSuccessData(T data, K key);

    void onFail(E error);
}
